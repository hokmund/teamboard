﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class TaskExecution
    {
        [Key, Column(Order = 1)]
        [ForeignKey("Sticker")]
        public int StickerId { set; get; }
        public virtual Sticker Sticker { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; } 
    }
}
