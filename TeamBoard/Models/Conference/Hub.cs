﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Configuration;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;
using Owin;
using TeamBoard.Models;
using DateTimeExtensions;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamBoard
{
    [NotMapped]
    class User
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
    }


    [NotMapped]
    [HubName("im")]
    public class Im : Hub
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // Список пользователей хаба на данный момент времени. 
        // User в данном контексте - это отдельный класс, описанный в этом файле.
        static List<User> Users = new List<User>();

        // Отправка сообщений
        public void Send(string name, string message, string conferenceId, string username)
        {
            // Время отправки сообщения.
            var now = DateTime.Now;
            int confId = Convert.ToInt32(conferenceId);
            var conference = db.Conferences.FirstOrDefault(chat => chat.Id == confId);
            var currentUser = db.Users.FirstOrDefault(user => user.UserName == username);
            string time = now.Format();
            // Отошлем сообщение сначала самому пользователю, а потом всем, кроме него.
            Clients.Caller.addMessage(name, message, time, true, currentUser.Userpic.Image);
            Clients.OthersInGroup(conferenceId).addMessage(name, message, time, false, currentUser.Userpic.Image);

            // Изменим порядок конференций в списке.
            var participants = db.Users.Where(user => user.Chattings.Any(chatting => chatting.ConferenceId == confId))
                .Select(x => x.UserName).ToList();
            var participantsConnections = Users.Where(user => participants.Any(x => x == user.Name)).Select(u => u.ConnectionId).ToList();
            Clients.Clients(participantsConnections).shuffleConferences(confId);
            
            // Создаем объект сообщения и добавляем в БД.
            db.Messages.Add(new Message
                {
                    LabelId = 1,
                    Conference = conference,
                    ConferenceId = conference.Id,
                    User = currentUser,
                    UserId = currentUser.Id,
                    Text = message,
                    Time = now
                });
            db.SaveChanges();
        }


        // Подключение нового пользователя
        public void Connect(string userName, string conferenceId)
        {
            var id = Context.ConnectionId;


            if (!Users.Any(x => x.ConnectionId == id))
            {
                Users.Add(new User { ConnectionId = id, Name = userName });
                this.Groups.Add(id, conferenceId);
                // Посылаем сообщение текущему пользователю
                Clients.Caller.onConnected(id, userName, Users);

                // Посылаем сообщение всем пользователям, кроме текущего
                // Clients.AllExcept(id).onNewUserConnected(id, userName);
            }
        }

        public void UpdateTime(string userName, string conferenceId)
        {
            var user = db.Users.FirstOrDefault(x => x.UserName == userName);
            user.Chattings.FirstOrDefault(x => x.ConferenceId == Convert.ToInt32(conferenceId) && x.UserId == user.Id)
                .TimeOfLastVisit = DateTime.Now;
            db.SaveChanges();
        }

        public void Disconnect(string userName, string conferenceId)
        {
            var id = Context.ConnectionId;
            if (Users.Any(x => x.ConnectionId == id))
            {
                Groups.Remove(id, conferenceId);
            }

            var user = db.Users.FirstOrDefault(x => x.UserName == userName);
            user.Chattings.FirstOrDefault(x => x.ConferenceId == Convert.ToInt32(conferenceId) && x.UserId == user.Id)
                .TimeOfLastVisit = DateTime.Now;
            db.SaveChanges();
        }

        // Отключение пользователя
        public override Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id, item.Name);
            }

            return base.OnDisconnected(stopCalled);
        }
	}       
}