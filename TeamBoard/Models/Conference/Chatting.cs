﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Chatting
    {
        [Key, Column(Order = 1)]
        [ForeignKey("Conference")]
        public int ConferenceId { set; get; }
        public virtual Conference Conference { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        public DateTime? TimeOfLastVisit { set; get; }
    }
}
