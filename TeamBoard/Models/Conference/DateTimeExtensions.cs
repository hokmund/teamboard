﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DateTimeExtensions
{
    public static class DateFormat
    {
        internal static string AddZeros(int p)
        {
            return p.ToString().Length > 1 ? p.ToString() : "0" + p.ToString();
        }

        public static string Format(this DateTime date)
        {
            return String.Format("{0}:{1}:{2}", AddZeros(date.Hour), AddZeros(date.Minute), AddZeros(date.Second));
        }
    }
}