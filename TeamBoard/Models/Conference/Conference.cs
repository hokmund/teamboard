﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Conference
    {
        public Conference()
        {
            Messages = new List<Message>();
            Chattings = new List<Chatting>();
        }

        [Key]
        [Column]
        public int Id { set; get; }

        [Column]
        [ForeignKey("Project")]
        public int ProjectId { set; get; }
        public virtual Project Project { set; get; }

        [Column]
        [ForeignKey("Creator")]
        public string CreatorId { set; get; }
        public virtual ApplicationUser Creator { set; get; }

        [Column]
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(25)]
        public string Title { set; get; }

        public virtual List<Message> Messages { set; get; }

        public virtual List<Chatting> Chattings { set; get; }
    }

    [NotMapped]
    public class ConferenceViewModel
    {
        public string Title { set; get; }
        public int Id { set; get; }
        public int Messages { set; get; }
    }
}
