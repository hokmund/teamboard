﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;

namespace TeamBoard.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
            : base()
        {
            Bugs = new List<Bug>();
            BugFixers = new List<BugFixer>();
            Chattings = new List<Chatting>();
            TaskExecutions = new List<TaskExecution>();
            Messages = new List<Message>();
            Participations = new List<Participation>();
            Projects = new List<Project>();
            Stickers = new List<Sticker>();
            Userpic = new Userpic();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Column]
        [Display(Name = "Country")]
        public string Country { set; get; }
        [Column]
        [Display(Name = "City")]
        public string City { set; get; }
        [Column]
        [Display(Name = "Name")]
        public string Name { set; get; }
        [Column]
        [Display(Name = "Gender")]
        public Sex Sex { set; get; }
        [Column]
        [DataType(DataType.Date)]
        [Display(Name = "Birthday")]
        public DateTime? Birthday { set; get; }

        [Column]
        public string Image
        {
            get
            {
                return Userpic.Image;
            }
        }

        public Userpic Userpic { set; get; }

        public virtual List<Bug> Bugs { set; get; }
        public virtual List<BugFixer> BugFixers { set; get; }
        public virtual List<Chatting> Chattings { set; get; }
        public virtual List<Conference> Conferences { set; get; }
        public virtual List<Sticker> Stickers { set; get; }
        public virtual List<Project> Projects { set; get; }
        public virtual List<Participation> Participations { set; get; }
        public virtual List<Message> Messages { set; get; }
        public virtual List<TaskExecution> TaskExecutions { set; get; }
    }

    public enum Sex
    { 
        Male, 
        Female,
        None
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Country> Countries { set; get; }
        public DbSet<Partition> Partitions { set; get; }
        public DbSet<Subpartition> Subpartitions { set; get; }
        public DbSet<Kanban> Kanbans { set; get; }
        public DbSet<Conference> Conferences { set; get; }
        public DbSet<Project> Projects { set; get; }
        public DbSet<Message> Messages { set; get; }
        public DbSet<Label> Labels { set; get; }
        public DbSet<Sticker> Stickers { set; get; }
        public DbSet<TaskExecution> TaskExecutions { set; get; }
        public DbSet<Chatting> Chattings { set; get; }
        public DbSet<Participation> Participations { set; get; }
        public DbSet<Bugtracker> Bugtrackers { set; get; }
        public DbSet<Bug> Bugs { set; get; }
        public DbSet<BugFixer> BugFixers { set; get; }
        public DbSet<BugFix> BugFixes { set; get; }
    }
}