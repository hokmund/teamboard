﻿using System.IO;

namespace TeamBoard.Models
{
    public class Userpic
    {
        static Userpic()
        {
            DEFAULT_USERPIC = "/../Pictures/Userpicks/default.png";
        }

        private static readonly string DEFAULT_USERPIC;
        private string _image;

        public string Image
        {
            get
            {
                return _image ?? DEFAULT_USERPIC;
            }
            set
            {
                if (ifPicture(value))
                _image = value;
            }
        }
        public bool IsDefault
        {
            get
            {
                return _image == null || _image == DEFAULT_USERPIC;
            }
        }

        private bool ifPicture(string path)
        {
            if(path.Substring(path.Length-4) == ".jpg" ||
                path.Substring(path.Length-4) == ".png" ||
                path.Substring(path.Length-5) == ".jpeg" ||
                path.Substring(path.Length - 4) == ".bmp")
            {
                return true;
            }
            return false;
        }
    }
}