﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Message
    {
        [Key]
        [Column]
        public int Id { set; get; }

        [Column]
        public string Text { set; get; }

        [Column]
        public DateTime? Time { set; get; }

        [Column]
        [ForeignKey("Label")]
        public int LabelId { set; get; }
        public virtual Label Label { set; get; }

        [Column]
        [ForeignKey("Conference")]
        public int ConferenceId { set; get; }
        public virtual Conference Conference { set; get; }

        [Column]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }
    }
}
