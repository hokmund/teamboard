﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Subpartition
    {
        public Subpartition()
        {
            Stickers = new List<Sticker>();
        }

        [Key]
        [Column]
        public int Id { set; get; }

        [Column]
        public int Position { get; set; }

        [Column]
        public string Header { get; set; }

        [Column]
        [ForeignKey("Partition")]
        public int PartitionId { set; get; }
        public virtual Partition Partition { set; get; }

        public virtual List<Sticker> Stickers { set; get; }
    }
}
