﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TeamBoard.Models
{
    public class BugFix
    {
        [Key]
        [Column]
        public int Id { set; get; }

        [Column]
        [ForeignKey("BugFixer")]
        public int BugFixerId { set; get; }
        public virtual BugFixer BugFixer { set; get; }

        [Column]
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Description { set; get; }
    }
}