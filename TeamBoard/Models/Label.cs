﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Label
    {
        public Label()
        {
            Stickers = new List<Sticker>();
            Messages = new List<Message>();
        }

        [Key]
        [Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [Column]
        public string Header { get; set; }

        [Column]
        public string Style { get; set; }

        [Column]
        [ForeignKey("Project")]
        public int? ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public virtual List<Sticker> Stickers { set; get; }

        public virtual List<Message> Messages { set; get; }
    }
}
