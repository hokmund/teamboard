﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Sticker
    {
        public Sticker()
        {
            TaskExecutions = new List<TaskExecution>();
        }

        [Key]
        [Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [Column]
        public int Position { get; set; }

        [Column]
        public string Description { get; set; }

        [Column]
        public string Header { get; set; }

        [Column]
        [ForeignKey("Subpartition")]
        public int SubpartitionId { set; get; }
        public virtual Subpartition Subpartition { set; get; }

        [Column]
        [ForeignKey("Label")]
        public int? LabelId { set; get; }
        public virtual Label Label { set; get; }

        [Column]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        public virtual List<TaskExecution> TaskExecutions { set; get; }
    }
}
