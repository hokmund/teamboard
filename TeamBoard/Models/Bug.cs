﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Bug
    {
        public Bug()
        {
            BugFixers = new List<BugFixer>();
        }

        [Key]
        [Column]
        public int Id { set; get; }

        [Column]
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Bug name")]
        public string Name { set; get; }

        [Column]
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Bug description")]
        public string Description { set; get; }

        [Column]
        [Required]
        public BugState State { set; get; } 

        [Column]
        [ForeignKey("Bugtracker")]
        public int BugtrackerId { set; get; }
        public virtual Bugtracker Bugtracker { set; get; }

        [Column]
        [ForeignKey("Reporter")]
        public string UserId { set; get; }
        public virtual ApplicationUser Reporter { set; get; }

        public virtual List<BugFixer> BugFixers { set; get; }
    }

    public enum BugState
    
    {
        [Display(Name="Needs confirmation", Description="Needs confirmation")]
        NeedsConfirmation,
        Open,
        Confirming,
        Closed
    }
}
