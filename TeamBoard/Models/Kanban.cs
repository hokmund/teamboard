﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Kanban
    {
        public Kanban()
        {
            Partitions = new List<Partition>();
        }

        [Key, ForeignKey("Project")]
        [Column]
        public int Id { set; get; }

        public virtual ICollection<Partition> Partitions { set; get; }

        
        public virtual Project Project { set; get; }
    }
}
