﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Bugtracker
    {
        public Bugtracker()
        {
            Bugs = new List<Bug>();
        }

        [Key]
        [Column]
        [ForeignKey("Project")]
        public int Id { set; get; }

        public virtual Project Project { set; get; }

        public virtual List<Bug> Bugs { set; get; }
    }
}
