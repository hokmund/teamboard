﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamBoard.Models
{
    public class Country
    {
        [Key]
        public int Id { set; get; }

        [Column]
        public string Word { set; get; }
    }
}
