﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Project
    {
        [NotMapped]
        public static readonly string[] DefaultLabelStyles = new string[] 
        { 
            "btn-default", "btn-primary", "btn-success", 
            "btn-info", "btn-warning", "btn-danger"
        };

        public Project()
        {
            Conferences = new List<Conference>();
            Participations = new List<Participation>();
            Labels = new List<Label>();

            //foreach (var style in DefaultLabelStyles)
            //{
            //    Labels.Add(new Label
            //        {
            //            Style = style,
            //            Header = "",
            //            Project = this
            //        });
            //}
        }

        [Key]
        [Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [Column]
        [Required]
        [DataType(DataType.Text)]
        [Display(Name="Project name")]
        public string Name { set; get; }

        [Column]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Description { set; get; }

        [Column]
        [DataType(DataType.Date)]
        [Display(Name = "Creation date")]
        public DateTime? Date { set; get; }

        [Column]
        [ForeignKey("Author")]
        public string AuthorId { set; get; }

        public virtual ApplicationUser Author { set; get; }

        public virtual Kanban Kanban { get; set; }

        public virtual Bugtracker Bugtracker { set; get; }

        public virtual List<Conference> Conferences { set; get; }

        public virtual List<Participation> Participations { set; get; }

        public virtual ICollection<Label> Labels { get; set; }
    }
}
