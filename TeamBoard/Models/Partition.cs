﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TeamBoard.Models
{
    public class Partition
    {
        public Partition()
        {
            Subpartitions = new List<Subpartition>();
        }

        [Key]
        [Column]
        public int Id { set; get; }

        [Column]
        public int Position { get; set; }

        [Column]
        public string Header { get; set; }

        [Column]
        [ForeignKey("Kanban")]
        public int KanbanId { set; get; }
        public virtual Kanban Kanban { set; get; }

        public virtual List<Subpartition> Subpartitions { set; get; }
    }
}