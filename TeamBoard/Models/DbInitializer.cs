﻿
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TeamBoard.Models
{
    public class DbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext db)
        {
            ApplicationUser Ilia = new ApplicationUser()
            {
                Id = "3ac19c9c-8fb5-4a71-9157-408a9004d27c",
                Name = "Shuteyev Ilia",
                Sex = Sex.Male,
                Email = "iliaking97@gmail.com",
                PasswordHash = "AKaXqQ1q6fPJXcoklRfohLxIIKino+7SiPY+P+i8s5cehJ8/g6vEdeEAoCXOq76VLg==",
                SecurityStamp = "ee0ae0e2-d60a-44b8-a042-8ac011762d1e",
                EmailConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = true,
                UserName = "Ilia"
            };
            db.Users.Add(Ilia);

            ApplicationUser Dmitriy = new ApplicationUser()
            {
                Id = "5ac19c9c-8fb5-4a71-9157-408a9004d27c",
                Name = "Dmitriy Panchenko",
                Sex = Sex.Male,
                Email = "ddd@gmail.com",
                PasswordHash = "AKaXqQ1q6fPJXcoklRfohLxIIKino+7SiPY+P+i8s5cehJ8/g6vEdeEAoCXOq76VLg==",
                SecurityStamp = "ee0ae0e2-d60a-44b8-a042-8ac011762d1e",
                EmailConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = true,
                UserName = "Dmitriy"
            };
            db.Users.Add(Dmitriy);

            ApplicationUser Andrey = new ApplicationUser()
            {
                Id = "6ac19c9c-8fb5-4a71-9157-408a9004d27c",
                Name = "Andrey Zaev",
                Sex = Sex.Male,
                Email = "zzz@gmail.com",
                PasswordHash = "AKaXqQ1q6fPJXcoklRfohLxIIKino+7SiPY+P+i8s5cehJ8/g6vEdeEAoCXOq76VLg==",
                SecurityStamp = "ee0ae0e2-d60a-44b8-a042-8ac011762d1e",
                EmailConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = true,
                UserName = "Andrey",
            };
            db.Users.Add(Andrey);

            Project project1 = new Project() 
            {
                Id = 1, 
                AuthorId = Ilia.Id, 
                Name = "Team Board", 
                Description = "Our project is at the top." 
            };
            db.Projects.Add(project1);

            Kanban kanban1 = new Kanban()
            {
                Id = 1
            };
            db.Kanbans.Add(kanban1);

            Participation participation1 = new Participation()
            {
                UserId = Ilia.Id,
                ProjectId = project1.Id,
                IsQA = true
            };
            db.Participations.Add(participation1);

            Participation participation2 = new Participation()
            {
                UserId = Dmitriy.Id,
                ProjectId = project1.Id,
                IsQA = true
            };
            db.Participations.Add(participation2);

            Participation participation3 = new Participation()
            {
                UserId = Andrey.Id,
                ProjectId = project1.Id,
                IsQA = false
            };
            db.Participations.Add(participation3);

            Bugtracker bugtracker1 = new Bugtracker()
            {
                Id = 1,
                Project = project1
            };
            project1.Bugtracker = bugtracker1;

            //Label label = new Label
            //{
            //    Id = 0
            //};
            //db.Labels.Add(label);

            //Дбавляем список стран в БД.
            string stringCountries = "Not Selected,Afghanistan,Albania,Algeria,Andorra,Angola,Antigua & Deps,Argentina,Armenia,Australia,Austria,Azerbaijan,Bahamas,Bahrain,Bangladesh,Barbados,Belarus,Belgium,Belize,Benin,Bhutan,Bolivia,Bosnia Herzegovina,Botswana,Brazil,Brunei,Bulgaria,Burkina,Burma,Burundi,Cambodia,Cameroon,Canada,Cape Verde,Central African Rep,Chad,Chile,People's Republic of China,Republic of China,Colombia,Comoros,Democratic Republic of the Congo,Republic of the Congo,Costa Rica,Croatia,Cuba,Cyprus,Czech Republic,Danzig,Denmark,Djibouti,Dominica,Dominican Republic,East Timor,Ecuador,Egypt,El Salvador,Equatorial Guinea,Eritrea,Estonia,Ethiopia,Fiji,Finland,France,Gabon,Gaza Strip,The Gambia,Georgia,Germany,Ghana,Greece,Grenada,Guatemala,Guinea,Guinea-Bissau,Guyana,Haiti,Holy Roman Empire,Honduras,Hungary,Iceland,India,Indonesia,Iran,Iraq,Republic of Ireland,Israel,Italy,Ivory Coast,Jamaica,Japan,Jonathanland,Jordan,Kazakhstan,Kenya,Kiribati,North Korea,South Korea,Kosovo,Kuwait,Kyrgyzstan,Laos,Latvia,Lebanon,Lesotho,Liberia,Libya,Liechtenstein,Lithuania,Luxembourg,Macedonia,Madagascar,Malawi,Malaysia,Maldives,Mali,Malta,Marshall Islands,Mauritania,Mauritius,Mexico,Micronesia,Moldova,Monaco,Mongolia,Montenegro,Morocco,Mount Athos,Mozambique,Namibia,Nauru,Nepal,Newfoundland,Netherlands,New Zealand,Nicaragua,Niger,Nigeria,Norway,Oman,Ottoman Empire,Pakistan,Palau,Panama,Papua New Guinea,Paraguay,Peru,Philippines,Poland,Portugal,Prussia,Qatar,Romania,Rome,Russian Federation,Rwanda,St Kitts & Nevis,St Lucia,Saint Vincent & the,Grenadines,Samoa,San Marino,Sao Tome & Principe,Saudi Arabia,Senegal,Serbia,Seychelles,Sierra Leone,Singapore,Slovakia,Slovenia,Solomon Islands,Somalia,South Africa,Spain,Sri Lanka,Sudan,Suriname,Swaziland,Sweden,Switzerland,Syria,Tajikistan,Tanzania,Thailand,Togo,Tonga,Trinidad & Tobago,Tunisia,Turkey,Turkmenistan,Tuvalu,Uganda,Ukraine,United Arab Emirates,United Kingdom,United States of America,Uruguay,Uzbekistan,Vanuatu,Vatican City,Venezuela,Vietnam,Yemen,Zambia,Zimbabwe";
            string[] countries = stringCountries.Split(new char[] { ','}, StringSplitOptions.RemoveEmptyEntries);
            int i = 1;
            foreach (string country in countries)
            {
                db.Countries.Add(new Country { Id = i, Word = country });
                i++;
            }
            db.SaveChanges();

            base.Seed(db);
        }
    }
}