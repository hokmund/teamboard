﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class BugFixer
    {
        [Key]
        public int Id { set; get; }

        [ForeignKey("Bug")]
        public int BugId { set; get; }
        public virtual Bug Bug { set; get; }

        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        public virtual List<BugFix> BugFixes { set; get; }
    }
}
