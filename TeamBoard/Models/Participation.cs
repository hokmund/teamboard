﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TeamBoard.Models
{
    public class Participation
    {
        [Key, Column(Order=1)]
        [ForeignKey("Project")]
        public int ProjectId { set; get; }
        public virtual Project Project { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        [Column]
        public bool IsQA { set; get; }
    }
}
