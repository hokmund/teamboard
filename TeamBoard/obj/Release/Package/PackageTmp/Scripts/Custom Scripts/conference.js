﻿
window.load = setTimeout(function () {
    var scroll = calculateBottomCoord();
    window.scrollTo(0, scroll);
    $('body, html').scrollTop($(document).height());
}, 250);

window.onbeforeunload = function (event) {
    var name = $('#username');
    var id = $('#conferenceId');
    chat.server.disconnect(name, id);
}
$(window).unload(function () {
    var name = $('#username');
    var id = $('#conferenceId');
    chat.server.disconnect(name, id);
});

window.onscroll = function () {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled == 0) {
        // Если пользователь доскроллил до верха, мы присылаем ему ещё 25 сообщений.

        $.ajax({
            url: DOWNLOAD_MESSAGES_ROUTE + "/" + $("#oldestMessage")[0].value,
            context: document.body,
            method: "POST",
            success: function (data) {
                // Изменим отметку о самом старом подгруженном сообщении.
                if (data.length > 0)
                {
                    $("#oldestMessage")[0].value = data[data.length - 1].id;
                }
                else
                {
                    $("#oldestMessage")[0].value = -1;
                }
                for (var i = 0; i < data.length; i++) {
                    var message = constructMessage(data[i].author, data[i].text, data[i].time, data[i].self, data[i].userpic, false);
                    $('#chatbody').prepend(message);
                }
            }
        });
    }
}

$(function () {

    // Ссылка на автоматически-сгенерированный прокси хаба.
    var chat = $.connection.im;

    // Объявление функции, которую хаб вызывает при получении сообщений.
    chat.client.addMessage = function (name, message, date, self, userpic) {
        // Фиксация скролла
        var size = Math.max(
  document.body.scrollHeight, document.documentElement.scrollHeight,
  document.body.offsetHeight, document.documentElement.offsetHeight,
  document.body.clientHeight, document.documentElement.clientHeight
);
        var posTop = window.pageYOffset || document.documentElement.scrollTop;
        posTop = window.innerHeight + posTop;
        var needScroll = (posTop == size);
        var result = constructMessage(name, message, date, self, userpic, !self);

        $('#chatbody').append(result);

        if (needScroll) {
            window.scrollTo(0, calculateBottomCoord());
        }
    };

    chat.client.shuffleConferences = function (id) {
        var confId = "#" + id + "chat";
        var conf = $(confId)[0];

        if (conf.className != "active") {
            var conference = conf.children[0];
            var number = parseInt(conference.children[0].firstChild.textContent);
            conference.children[0].firstChild.textContent = number + 1;
        }

        $("#listBeginning").prepend($(confId)[0]);
    };

    // Функция, вызываемая при подключении нового пользователя.
    chat.client.onConnected = function (id, userName, allUsers) {


        // Добавление всех пользователей.
        for (i = 0; i < allUsers.length; i++) {

            AddUser(allUsers[i].ConnectionId, allUsers[i].Name);
        }
    }

    // Добавляем нового пользователя
    chat.client.onNewUserConnected = function (id, name) {

        AddUser(id, name);
    }

    // Удаляем пользователя
    chat.client.onUserDisconnected = function (id, userName) {

        $('#' + id).remove();
    }

    // Открываем соединение
    $.connection.hub.start().done(function () {

        var name = $('#username').val();
        var id = $('#conferenceId');

        $('#sendmessage').click(function () {
            // Пустое или состоящее из пробельных символов сообщение не должно отправляться.
            if ($('#message').val().trim() == '') {
                return;
            }

            // Вызываем у хаба метод Send
            chat.server.send($('#realName').val(), $('#message').val(), id.val(), name);
            $('#message').val('');
        });

        chat.server.connect(name, id.val());

        
    });


    $("#chatbody")[0].onmousemove = function () {
        $(".direct-chat-msg.unread").removeClass("unread");
        chat.server.updateTime($('#username').val(), $('#conferenceId').val());
    }


});
// Кодирование тегов.
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}
// Отправка сообщения по Enter.
document.onkeydown = function (e) {
    e = e || window.event;
    if (e.keyCode === 13) {
        $('#sendmessage').click();
        return false;
    }
}


// Добавление нового пользователя.
function AddUser(id, name) {

    var userId = $('#hdId').val();

    if (userId != id) {
        $("#chatusers").append('<p id="' + id + '">' + name + '</p>');
    }
}

function calculateBottomCoord() {
    var scroll = window.pageYOffset || document.documentElement.scrollTop;
    scroll = window.innerHeight + scroll;
    return scroll;
}


function constructMessage(name, message, date, self, userpic, unread) {
    // Создание сообщений согласно шаблону.
    var resultedName = "<div class='direct-chat-msg";
    if (unread) {
        resultedName += " unread";
    }
    //resultedName += "' onmouseenter='markAsRead()'>";
    resultedName += "'>";
    if (self) {
        resultedName = resultedName + '<div class="direct-chat-info clearfix my-msg">';
    }
    else {
        resultedName = resultedName + "<div class='direct-chat-info clearfix'>";
    }
    resultedName += '<span class="direct-chat-name pull-left">' + htmlEncode(name) + '</span>';
    result = resultedName + '<span class="direct-chat-timestamp pull-right">' + htmlEncode(date)
        + '</span></div><img class="direct-chat-img" src="' + userpic
        + '" alt="Username"><div class="direct-chat-text">'
        + htmlEncode(message) + '</div></div>';

    return result;
}

