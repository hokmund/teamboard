﻿function addColumnButtonClick(e) {
    var element = $("<div></div>",
        {
            class: "kanban-column"
        });

    element.append($("<div>", { class: "kanban-column-header" })
                .append($("<h3>", { text: "Division" }).dblclick(editColumnHeader))
                .append($("<i>", { class: "fa fa-close fa-lg kanban-column-close", id: "newColumn" }).click(deleteColumn)))
           .append($("<div>", { class: "btn btn-default subpartition-add-btn", text: "Add subdivision" }).click(addSubpartition))
           .append($("<div>", { class: "subpartition" })
               .append($("<div>", { class: "sticker-add-btn sticker-hidden-btn" })
                    .append($("<div>", { class: "btn btn-default" }).click(addSticker)
                        .append($("<i>", { class: "fa fa-plus" }))
                        .append("Add sticker")))
               .append($("<input>", { type: "hidden", class: "subpartition-id", value: undefined, id: "newSubpartitionId" })))
           .append($("<input>", { type: "hidden", class: "partition-id", id: "newColumnId" }));

    //element.append("<div class=\"kanban-column-header\"><h3>Column</h3></div>");
    //element;
    //element.append("<div class=\"sticker-add-btn sticker-hidden-btn\"><div class=\"btn btn-default\"><i class=\"fa fa-plus\"></i>Add sticker</div></div>")

    lastColumn = element;
    element.insertBefore("#addColumnButton")
    //$("<hr/>",
    //    {
    //        class: "vr"
    //    }).appendTo(element);
    $.ajax({
        url: ADD_COLUMN_ROUTE + "/" + $("#id")[0].value,
        context: document.body,
        method: "POST",
        success: columnAdded
    });

    updateSortables();
}

function columnAdded(data) {
    $(".kanban-column-close")[data.position].id = data.id;
    $(".partition-id")[data.position].value = data.id;
    $(".partition-id")[data.position].id = "";
    $($(".partition-id")[data.position]).parent().find(".subpartition-id")[0].value = data.subId;
}

function deleteColumn(e) {
    var id = $(e.currentTarget).parent().parent().children(".partition-id")[0].value;
    $(e.currentTarget).parent().parent().remove();

    $.ajax({
        url: DELETE_COLUMN_ROUTE,
        context: document.body,
        method: "POST",
        data: { id: id }
    });
}

function replaceColumn(e, ui) {
    var id = $(ui.item).children(".partition-id")[0].value;

    //window.alert(id.toString() + $(ui.item).index());
    $.ajax(
        {
            url: REPLACE_COLUMN_ROUTE,
            data: { id: id, newPosition: $(ui.item).index() },
            context: document.body,
            method: "POST"
        });
}

function addSubpartition(e) {
    var sub = $("<div>", { class: "subpartition ui-sortable" })
        .append($("<div>", { class: "subpartition-header" }).dblclick(editSubpartitionHeader)
            .append($("<h4>", { text: "Subdivision" } ))
            .append($("<i>", { class: "fa fa-remove subpartition-remove-btn" }).click(removeSubpartition)))
        .append($("<div>", { class: "sticker-add-btn sticker-hidden-btn" })
            .append($("<div>", { class: "btn btn-default" }).click(addSticker)
                .append($("<i>", { class: "fa fa-plus" }))
                .append("Add sticker")))
        .append($("<input>", { type: "hidden", class: "subpartition-id", value: undefined, id: "newSubpartitionId" }));

    var button = $(e.currentTarget);
    if (button.parent().children(".subpartition").length == 1) {

        button.parent().children(".subpartition")
            .prepend($("<div>", { class: "subpartition-header" }).dblclick(editSubpartitionHeader)
                .append($("<h4>", { text: "Subdivision" }))
                .append($("<i>", { class: "fa fa-remove subpartition-remove-btn" }).click(removeSubpartition)));

    }
    sub.insertAfter(button.parent().children(".subpartition").last());

    var id = button.parent().children(".partition-id")[0].value;

    $.ajax(
        {
            url: ADD_SUBPARTITION_ROUTE,
            method: "POST",
            context: document.body,
            data: { columnId: id },
            success: subpartitionAdded
        });

    updateSortables();
}

function subpartitionAdded(data) {
    $($($(".kanban-column")[data.columnPosition])
        .children(".subpartition")[data.subpartitionPosition])
        .children(".subpartition-id")[0].value = data.id;
}

function editSubpartitionHeader(e) {
    if ($(e.currentTarget).children(".name-editor").length != 0) {
        return;
    }
    var editor = $("<input>",
        {
            type: "textbox",
            value: $(e.currentTarget).children("h4").text().trim(),
            class: "name-editor",
        })
        .focusout(saveSubpartitionHeader)
        .keydown(function (e) {
            var code = e.which;
            if (code == 13) {
                saveSubpartitionHeader(e)
            }
        })
    $(e.currentTarget).children().remove();
    $(e.currentTarget).prepend(editor);
    //$(e.currentTarget).addClass("hidden-header");
    editor.focus().select();
}

function saveSubpartitionHeader(e) {
    var header = e.currentTarget.value.trim();
    if (header.length != 0) {
        var jElement = $(e.currentTarget);
        var id = jElement.parent().parent().children(".subpartition-id")[0].value;
        if ($.isNumeric(id)) {
            //jElement.parent().children("h3").removeClass("hidden-header").text(header);
            $(e.currentTarget).parent()
                    .append($("<h4>", { text: header }))
                    .append($("<i>", { class: "fa fa-remove fa-lg subpartition-remove-btn" })
                        .click(removeSubpartition));
            $(e.currentTarget).remove();


            $.ajax({
                url: RENAME_SUBPARTITION_ROUTE,
                context: document.body,
                method: "POST",
                data: { subpartitionId: id, header: header }
            });
        }

    }

}

function removeSubpartition(e) {
    var id = $(e.currentTarget).parent().parent().children(".subpartition-id")[0].value;

    $.ajax(
        {
            url: DELETE_SUBPARTITION_ROUTE,
            method: "POST",
            context: document.body,
            data: { subpartitionId: id }
        });

    var column = $(e.currentTarget).parent().parent().parent();
    $(e.currentTarget).parent().parent().remove();

    if (column.children(".subpartition").length == 1) {
        column.children(".subpartition").children(".subpartition-header").hide();
    }


}

function replaceSubcolumn(e, ui) {
    var id = $(ui.item).children(".subpartition-id")[0].value;

    $.ajax(
        {
            url: REPLACE_SUBCOLUMN_ROUTE,
            data: {
                id: id,
                newPosition: $(ui.item).parent().children(".subpartition").index($(ui.item)),
                columnId: $(ui.item).parent().children(".partition-id")[0].value
            },
            context: document.body,
            method: "POST"
        });

    var columns = $("#kanban").children(".kanban-column");
    for (var i = 0; i < columns.length; i++) {
        if ($(columns[i]).children(".subpartition").length == 1) {
            $(columns[i]).children(".subpartition").children(".subpartition-header").hide();
        }
        else {
            var subcolumns = $(columns[i]).children(".subpartition");
            for (var j = 0; j < subcolumns.length; j++) {
                if ($(subcolumns[j]).is($(ui.item))) {
                    continue;
                }
                if ($(subcolumns[j]).children(".subpartition-header").length == 0) {
                    $(subcolumns[j]).prepend($("<div>", { class: "subpartition-header" }).dblclick(editSubpartitionHeader)
                        .append($("<h4>", { text: "Subdivision" }))
                        .append($("<i>", { class: "fa fa-remove subpartition-remove-btn" }).click(removeSubpartition)));
                }
                else {
                    $(subcolumns[j]).children(".subpartition-header").show();
                }

            }
        }
    }
}

function addSticker(e) {
    var div = $("<div>")
                .append($("<div>", { class: "sticker-editor" })
                    .append($("<textarea>", { class: "sticker-header-editor" }).focus())
                    .append($("<div>", { class: "btn btn-default", text: "Cancel", style: "margin-left: 20px;" }).click(cancelSticker))
                    .append($("<div>", { class: "btn btn-primary", text: "Add", style: "margin-left: 10px;" }).click(saveSticker)));
    div.insertBefore($(e.currentTarget).parent());
    $(e.currentTarget).parent().hide();
    setTimeout(function () {
        div.find("textarea").focus();
    }, 0);
}

function saveSticker(e) {
    //button -> sticker editor <- !(header editor)
    var text = $(e.currentTarget).parent().children(".sticker-header-editor")[0].value.trim();

    if (text.length != 0) {
        //button -> sticker editor -> sticker -> subpartition <- !(hidden id input)
        var id = $(e.currentTarget).parent().parent().parent().children("input.subpartition-id")[0].value;

        $.ajax({
            url: ADD_STICKER_ROUTE,
            context: document.body,
            data: { subpartitionId: id, header: text },
            method: "POST",
        })
        $(e.currentTarget).parent().parent().addClass("sticker");
        $(e.currentTarget).parent().parent().append($("<h4>", { text: text }));
        $(e.currentTarget).parent().parent().click(showInfo);
        $(e.currentTarget).parent().parent().parent().children(".sticker-add-btn").show();
        $(e.currentTarget).parent().remove();
        e.stopPropagation();

        $(".subpartition").sortable(
            {
                connectWith: ".subpartition",
                items: ".sticker",
                receive: function (e, ui) {
                    if ($(ui.item).index() == $(ui.item).parent().children().length - 1) {
                        //$(ui.sender).sortable("cancel");
                        $(ui.item).insertBefore($(ui.item).parent().children(".sticker-add-btn"));
                    }
                },
                out: stickerOut,
                update: replaceSticker,
                start: stickerSortStart
            });
    }
}

function cancelSticker(e) {
    //button -> sticker editor -> !(sticker)

    $(e.currentTarget).parent().parent().parent().children(".sticker-add-btn").show();
    $(e.currentTarget).parent().parent().remove();
}

var fromSubcolumnId;
var fromSubcolumnPosition;
function replaceSticker(e, ui) {
    if (fromSubcolumnPosition == undefined) {
        return;
    }

    var toSubcolumnId = $(ui.item).parent().children(".subpartition-id")[0].value;

    if (fromSubcolumnId == undefined) {
        fromSubcolumnId = toSubcolumnId;
    }

    $.ajax(
        {
            url: REPLACE_STICKER_ROUTE,
            data: {
                fromSubpartitionId: fromSubcolumnId,
                oldPosition: fromSubcolumnPosition,
                toSubpartitionId: toSubcolumnId,
                newPosition: $(ui.item).parent().children(".sticker").index($(ui.item)),
            },
            context: document.body,
            method: "POST"
        });

    if (fromSubcolumnId == toSubcolumnId) {
        if ($(ui.item).parent().children(".sticker").length == 1) {
            var sticker = $(ui.item);
            sticker.insertBefore($($(ui.item).parent().children(".sticker-add-btn")));
        }
    }

    fromSubcolumnId = undefined;
    fromSubcolumnPosition = undefined;
}

function stickerSortStart(e, ui) {
    fromSubcolumnPosition = $(ui.item).parent().children(".sticker").index($(ui.item));
}

function stickerOut(e, ui) {
    fromSubcolumnId = $(ui.item).parent().children(".subpartition-id")[0].value;
}

function editColumnHeader(e) {
    var editor = $("<input>",
        {
            type: "textbox",
            value: $(e.currentTarget).text().trim(),
            class: "name-editor",
        })
        .focusout(saveColumnHeader)
        .keydown(function (e) {
            var code = e.which;
            if (code == 13) {
                saveColumnHeader(e);
            }
        });

    $(e.currentTarget).parent().children("h3")
        .replaceWith(editor);
    //$(e.currentTarget).addClass("hidden-header");
    editor.focus().select();
}

function saveColumnHeader(e) {
    var header = e.currentTarget.value.trim();
    if (header.length != 0) {
        var jElement = $(e.currentTarget);
        var id = jElement.parent().parent().children("input.partition-id")[0].value;
        if ($.isNumeric(id)) {
            //jElement.parent().children("h3").removeClass("hidden-header").text(header);
            $(e.currentTarget).replaceWith($("<h3>", { text: header }).dblclick(editColumnHeader));


            $.ajax({
                url: RENAME_COLUMN_ROUTE + "?id=" + id + "&header=" + header, context: document.body,
                method: "POST"
            });
        }
                
    }

}

var isEmptyDescription;
var stickerInfo;
var shownSticker;

function showInfo(e) {
    var id = $(e.currentTarget).parent().children("input.subpartition-id")[0].value;
    var position = $(e.currentTarget).parent().children(".sticker").index($(e.currentTarget));
    shownSticker = $(e.currentTarget);
    $.ajax({
        url: GET_STICKER_INFO_ROUTE,
        context: document.body,
        data: { subpartitionId: id, position: position },
        method: "POST",
        complete: infoGettingCompleted
    });
}

function infoGettingCompleted(data, status) {

    $("#stickerInfoHeader").text(data.responseJSON.Header);
    if (data.responseJSON.Description == null) {
        $("#stickerInfoDescription").text("There is no description.");
        isEmptyDescription = true;
    }
    else {
        $("#stickerInfoDescription").text(data.responseJSON.Description);
        isEmptyDescription = false;
    }
    $("#stickerId")[0].value = data.responseJSON.Id;

    $("#executors").children().remove();

    if (data.responseJSON.IsTasked) {
        setExecuting($("#executeBtn"));
    }
    else {
        setNonExecuting($("#executeBtn"));
    }

    for (var i = 0; i < data.responseJSON.Executors.length; i++) {
        $("#executors").append($("<div>", { text: data.responseJSON.Executors[i] }));
    }

    $("#labels").children().remove();

    $("#labels").append($("<li>")
        .append($("<span>", { text: "none", class: "btn btn-default sticker-label" })
            .click(setLabel)));
    for (var i = 0; i < data.responseJSON.AllLabels.length; i++) {
        var labelSpecificClass = data.responseJSON.AllLabels[i].Style
                        + (data.responseJSON.Label == null
                            ? ""
                            : (data.responseJSON.Label.Id == data.responseJSON.AllLabels[i].Id
                                ? " selected-label"
                                : ""));

        $("#labels").append($("<li>").append($("<span>",
            {
                text: data.responseJSON.AllLabels[i].Header + "                                                 ",
                class: "btn sticker-label " + labelSpecificClass
            }).click(setLabel)));
    }

    if (data.responseJSON.Label == null) {
        //$("#labels").children().first().append($("<i>", { class: "fa fa-check" }));
        $("#labels").children().children().first().addClass("selected-label");
    }
    var labels = $("#labels").children();
    for (var i = 0; i < labels.length; i++) {
        if ($(labels[i]).children().hasClass("selected-label")) {
            $(labels[i]).append($("<i>", { class: "fa fa-check" }));
        }
    }

    //if (data.responseJSON.Executors.length == 0) {
    //    $("#executors").append($("<div>", { text: "none", id: "none" }));
    //}

    stickerInfo = data.responseJSON;



    var dialog = $("#stickerInfo").dialog({
        autoOpen: false,
        height: 400,
        width: 600,
        modal: true,
        buttons:[
            {
                text: "Close",
                class: "btn btn-default",
                click: function () {
                    dialog.dialog("close");
                }
            }]
    });
    dialog.dialog("open");
}

function setLabel(e) {
    $("#labels").children().children("i").remove();

    $(e.currentTarget).parent().append($("<i>", { class: "fa fa-check" }));

    var labelId;
    if ($(e.currentTarget).parent().index() - 1 != -1) {
        labelId = stickerInfo.AllLabels[$(e.currentTarget).parent().index() - 1].Id;
    }
    else {
        labelId = null;
    }

    $.ajax(
        {
            url: SET_STICKER_LABEL_ROUTE,
            method: "POST",
            context: document.body,
            data: { stickerId: stickerInfo.Id, labelId: labelId }
        });
    shownSticker.children(".sticker-label").remove();

    if (labelId != null) {
        shownSticker.prepend($("<div>", { class: e.currentTarget.className }));
    }

}

function editStickerHeader() {
    var editor = $("<input>",
        {
            type: "textbox",
            value: $("#stickerInfoHeader").text().trim(),
            id: "stickerInfoHeaderEditor"
        })
        .focusout(function (e) {
            var header = e.currentTarget.value.trim();
            if (header.length != 0) {
                var jElement = $(e.currentTarget);
                //jElement.parent().children("h3").removeClass("hidden-header").text(header);
                $(e.currentTarget)
                    .replaceWith(header);

                shownSticker.children("h4").text(header);

                $.ajax({
                    url: RENAME_STICKER_ROUTE,
                    context: document.body,
                    method: "POST",
                    data: { id: stickerInfo.Id, header: header }
                });

            }

        })
    $("#stickerInfoHeader").text("");
    $("#stickerInfoHeader").append(editor);
    //$(e.currentTarget).addClass("hidden-header");
    editor.focus().select();
}

function editInfo(e) {
    if ($("#stickerInfoEditBtn").text().trim() == "Edit description") {
        $("#stickerInfoDescription").replaceWith($("<textarea>",
            { text: (!isEmptyDescription ? $("#stickerInfoDescription").text() : ""), id: "stickerInfoEditor" }).focus())
        $("#stickerInfoEditBtn").removeClass("btn-default").addClass("btn-primary").text("Save");
    }
    else {
        var text = $("#stickerInfoEditor")[0].value;
        if (text.length != 0 && text.trim().length == 0) {
            $("#stickerInfoEditor").text(text.trim());
            return;
        }
        text = text.trim();

        $.ajax({
            url: SET_STICKER_DESCRIPTION_ROUTE,
            context: document.body,
            method: "POST",
            data: { id: $("#stickerId")[0].value, description: text },
            complete: function () {
                $("#stickerInfoEditor").replaceWith($("<p>", {
                    id: "stickerInfoDescription",
                    text: (text.length != 0 ? text : "There is no description.")
                }));
                $("#stickerInfoEditBtn").removeClass("btn-primary").addClass("btn-default").text("Edit");

            }
        })
    }

}

function deleteSticker() {
    $("#stickerInfo").dialog("close");

    //var subcolumn = $(".subpartition").filter(function(index) 
    //{ 
    //    return $(this).children(".subpartition-id")[0].value == stickerInfo.SubpartitionId;
    //})
    shownSticker.remove();
    $.ajax(
        {
            url: DELETE_STICKER_ROUTE,
            context: document.body,
            data: { id: stickerInfo.Id },
            method: "POST",
        });
}

function changeExecuting(e) {
    var button = $(e.currentTarget);
    var isExecuting = undefined;
    if (button.hasClass("btn-warning")) {
        setExecuting(button);
        isExecuting = true;
    }
    else if (button.hasClass("btn-success")) {
        setNonExecuting(button);
        isExecuting = false;
    }

    $.ajax(
        {
            url: SET_EXECUTING_STICKER_ROUTE,
            context: document.body,
            data: { id: stickerInfo.Id, isExecuting: isExecuting },
            method: "POST",
        });

}

function setExecuting(button) {
    button.addClass("btn-success");
    button.removeClass("btn-warning");
    button.children("i").removeClass("fa-remove").addClass("fa-check");
    button.children("span").text("Executing");

    $("#executors").prepend($("<div>", {
        style: "text-decoration: underline;",
        text: "You",
        id: "userExecutor"
    }));

    $("#none").remove();
}

function setNonExecuting(button) {
    button.removeClass("btn-success");
    button.addClass("btn-warning");
    button.children("i").removeClass("fa-check").addClass("fa-remove");
    button.children("span").text("Not executing");

    $("#userExecutor").remove();
    if ($("#executors").children().length == 0) {
        $("#executors").append($("<div>", { text: "none", id: "none" }));
    }
}

function updateSortables() {
    $("#kanban").sortable(
        {
            connectWith: "#kanban",
            handle: ".kanban-column-header",
            update: replaceColumn,
            items: ".kanban-column",
            cancel: ".kanban-column-close"
        });

    $(".kanban-column").sortable(
        {
            connectWith: ".kanban-column",
            handle: ".subpartition-header",
            cancel: ".subpartition-remove-btn",
            update: replaceSubcolumn,
            items: ".subpartition"
        });
    $(".subpartition").sortable(
        {
            connectWith: ".subpartition",
            items: ".sticker",
            receive: function (e, ui) {
                if ($(ui.item).index() == $(ui.item).parent().children().length - 1) {
                    //$(ui.sender).sortable("cancel");
                    $(ui.item).insertBefore($(ui.item).parent().children(".sticker-add-btn"));
                }
            },
            out: stickerOut,
            update: replaceSticker,
            start: stickerSortStart
        })
}

$(document).ready(function () {
    $("#addColumnButton").click(addColumnButtonClick);
    $(".kanban-column-header>i").click(deleteColumn);

    //$(".kanban-column")
    //    .mouseenter(function (e) {
    //        $(e.currentTarget).children(".sticker-add-btn").children().removeClass("sticker-hidden-btn");
    //    })
    //    .mouseleave(function (e) {
    //        $(e.currentTarget).children(".sticker-add-btn").children().addClass("sticker-hidden-btn");
    //    });

    $(".kanban-column-header>h3").dblclick(editColumnHeader);

    $(".subpartition-add-btn").click(addSubpartition);

    $(".subpartition-header").dblclick(editSubpartitionHeader);

    $(".subpartition-remove-btn").click(removeSubpartition);

    $(".sticker-add-btn>div").click(addSticker);

    $(".sticker").click(showInfo)

    $("#stickerInfoEditBtn").click(editInfo)

    $("#deleteBtn").click(deleteSticker);
    $("#executeBtn").click(changeExecuting);

    $("#stickerInfoHeader").dblclick(editStickerHeader);
});

$(function () {
    updateSortables();
});