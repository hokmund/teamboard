﻿function enableConf() {

    onChatReady();  

    // Ссылка на автоматически-сгенерированный прокси хаба.
    var chat = $.connection.im;

    // Объявление функции, которую хаб вызывает при получении сообщений.
    chat.client.addMessage = function (name, message, date, self, userpic) {
        // Фиксация скролла
        debugger;
        var height = document.getElementById("conferences-sidebar").scrollHeight;
        var pos = $("#conferences-sidebar").scrollTop() + $("#conferences-sidebar").height();
        var needScroll = height == pos;
        var chatbody = $("#chatbody");
        var result = constructMessage(name, message, date, self, userpic, !self);
        chatbody.append(result);

        // Вернем скролл на место, если нужно.
        if (needScroll) {
            onChatReady();
        }
    };

    // Функция, вызываемая при подключении нового пользователя.
    chat.client.onConnected = function (id, userName, allUsers) {


        // Добавление всех пользователей.
        for (i = 0; i < allUsers.length; i++) {

            AddUser(allUsers[i].ConnectionId, allUsers[i].Name);
        }
    }

    // Добавляем нового пользователя
    chat.client.onNewUserConnected = function (id, name) {

        AddUser(id, name);
    }

    // Удаляем пользователя
    chat.client.onUserDisconnected = function (id, userName) {

        $('#' + id).remove();
    }

    // Открываем соединение
    $.connection.hub.start().done(function () {

        
            var name = $('#username').val();
            var id = $('#conferenceId');

            $('#sendmessage').click(function () {
                // Вызываем у хаба метод Send
                if ($('#message').val().trim() == '') {
                    return;
                }
                chat.server.send($('#realName').val(), $('#message').val(), id.val(), name);
                $('#message').val('');
            });

            chat.server.connect(name, id.val());
       

    });

    window.onbeforeunload = function () {
        var name = $('#username');
        var id = $('#conferenceId');
        chat.server.disconnect(name, id);
    }

    $('#loadMoreButton').click(function loadMore() {

        $.ajax({
            url: DOWNLOAD_MESSAGES_ROUTE + "/" + $("#oldestMessage")[0].value,
            context: document.body,
            method: "POST",
            success: loaded
        });
    });

    $("#chatbody")[0].onmousemove = function () {
        $(".unread").removeClass("unread");
        chat.server.updateTime($('#username').val(), $('#conferenceId').val());
    }
}

    

    function loaded(data) {
        // Изменим отметку о самом старом подгруженном сообщении.
        //debugger;
        if (data.length > 0) {
            $("#oldestMessage")[0].value = data[data.length - 1].id;
        }
        else {
            $("#oldestMessage")[0].value = -1;
        }
        for (var i = 0; i < data.length; i++) {
            var message = constructMessage(data[i].author, data[i].text, data[i].time, data[i].self, data[i].userpic, false);
            $("#chatInnerBody").prepend(message);
            //message += document.getElementById('chatInnerBody').innerHTML;
            //if (i == 0)
            //    alert(message);
            //document.getElementById('chatInnerBody').innerHTML = message;
            //document.getElementById('chatInnerBody').innerHTML += data[i].text;
            //if (i == 0)
            //    alert(document.getElementById('chatInnerBody').innerHTML);
        }
        if (data.length < 25) {
            $("#loadMoreButton").addClass("disabled");
        }
    }


    // Кодирование тегов.
    function htmlEncode(value) {        
        return value;
    }

    // Отправка сообщения по Enter.
    document.onkeydown = function (e) {
        e = e || window.event;
        if (e.keyCode === 13) {
            $('#sendmessage').click();
            return false;
        }
    }


    // Добавление нового пользователя.
    function AddUser(id, name) {

        var userId = $('#hdId').val();

        if (userId != id) {
            $("#chatusers").append('<p id="' + id + '">' + name + '</p>');
        }
    }

    function calculateBottomCoord() {
        var scroll = window.pageYOffset || document.documentElement.scrollTop;
        scroll = window.innerHeight + scroll;
        return scroll;
    }


    function constructMessage(name, message, date, self, userpic, unread) {
        // Создание сообщений согласно шаблону.
        //var resultedName = "<div class='direct-chat-msg";
        //if (unread) {
        //    resultedName += " unread";
        //}
        ////resultedName += "' onmouseenter='markAsRead()'>";
        //if (self) {
        //    resultedName = resultedName + '<div class="direct-chat-info clearfix my-msg">';
        //}
        //else {
        //    resultedName = resultedName + "<div class='direct-chat-info clearfix'>";
        //}
        //resultedName += '<span class="direct-chat-name pull-left">' + name + '</span>';
        //result = resultedName + '<span class="direct-chat-timestamp pull-right">' + date
        //    + '</span></div><img class="direct-chat-img" src="' + userpic
        //    + '" alt="Username"><div class="direct-chat-text">'
        //    + message + '</div></div>';

        var messageBlock =
            $("<div>", { class: "direct-chat-msg" + (unread ? " unread" : "") })
                .append($("<div>", { class: "direct-chat-info clearfix" + (self ? " my-msg" : "") })
                    .append($("<span>", { class: "direct-chat-name pull-left", text: name }))
                    .append($("<span>", { class: "direct-chat-timestamp pull-right", text: date })))
                .append($("<img>", { class: "direct-chat-img", src: userpic, alt: "Username" }))
                .append($("<div>", { class: "direct-chat-text", text: message }));

        return messageBlock;
    }

    function onChatReady() {
        var block = document.getElementById('conferences-sidebar');
        block.scrollTop = block.scrollHeight;
    }

    function markAsRead() {
        $(".direct-chat-msg unread").removeClass("unread");
    }