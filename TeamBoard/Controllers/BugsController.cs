﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TeamBoard.Models;

namespace TeamBoard.Controllers
{
    [Authorize]
    public class BugsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        public ActionResult Index()
        {
            var bugs = db.Bugs.Include(b => b.Bugtracker).Include(b => b.Reporter);
            return View(bugs.ToList());
        }

        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bug bug = db.Bugs.Find(id);
            if (bug == null)
            {
                return HttpNotFound();
            }
            return PartialView("DetailsPartial", bug);
        }

        [Authorize]
        public ActionResult Create(int bugTrackerId)
        {
            ViewBag.BugtrackerId = bugTrackerId;
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Bug bug)
        {
            bug.UserId = db.Users.First(user => user.UserName == User.Identity.Name).Id;
            bug.State = BugState.NeedsConfirmation;
            if (ModelState.IsValid)
            {
                db.Bugs.Add(bug);
                db.SaveChanges();
                bug = db.Bugs.First(b => b.Id == bug.Id);
                return RedirectToAction("ShowBugtracker", "Bugs", new { id = db.Bugtrackers.First(bt => bt.Id == bug.BugtrackerId).Id });
            }

            ViewBag.BugtrackerId = bug.BugtrackerId;
            return View(bug);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bug bug = db.Bugs.Find(id);
            if (bug == null)
            {
                return HttpNotFound();
            }
            return View(bug);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Bug bug)
        {
            if (ModelState.IsValid)
            {
                db.Bugs.First(b => b.Id == bug.Id).Name = bug.Name;
                db.Bugs.First(b => b.Id == bug.Id).Description = bug.Description;
                db.SaveChanges();
                bug = db.Bugs.First(b => b.Id == bug.Id);
                return RedirectToAction("ShowBugtracker", "Bugs", new { id = db.Bugtrackers.First(bt => bt.Id == bug.BugtrackerId).Id });
            }
            return View(bug);
        }

        [Authorize]
        public ActionResult ShowBugtracker(int id)
        {
            Bugtracker bugtracker = db.Bugtrackers.Find(id);
            if (bugtracker == null)
            {
                return HttpNotFound();
            }

            ViewBag.AddedTesters = db.Users.Where(user => user.Participations.Any(participation => participation.ProjectId == bugtracker.Project.Id && participation.IsQA)).ToList();
            return View(bugtracker);
        }

        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bug bug = db.Bugs.Find(id);
            if (bug == null)
            {
                return HttpNotFound();
            }
            return View(bug);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bug bug = db.Bugs.Find(id);
            db.Bugs.Remove(bug);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //[HttpPost]
        public ActionResult MakeOpenBug(int bugId)
        {
            Bug b = db.Bugs.First(bug => bug.Id == bugId);
            b.State = BugState.Open;
            db.SaveChanges();
            return RedirectToAction("ShowBugtracker", "Bugs", new { id = b.BugtrackerId });
        }

        [HttpPost]
        public ActionResult MakeClosedBug(int bugId)
        {
            Bug b = db.Bugs.First(bug => bug.Id == bugId);
            b.State = BugState.Closed;
            db.SaveChanges();
            return RedirectToAction("ShowBugtracker", "Bugs", new { id = b.BugtrackerId });
        }

        [HttpPost]
        public ActionResult CreateTextAreaForFix(int bugId)
        {
            ViewBag.bugId = bugId;
            return PartialView("AddBugFix");
        }

        [HttpPost]
        public ActionResult AddFix(string description, int bugId)
        {
            BugFix bugFix = new BugFix()
            {
                Description = description,
                BugFixerId = db.BugFixers.First(bugFixer => 
                    bugFixer.User.UserName == User.Identity.Name 
                    && bugFixer.BugId==bugId).Id
            };
            db.BugFixes.Add(bugFix);

            Bug bug = db.Bugs.First(b => b.Id == bugId);
            bug.State = BugState.Confirming;

            db.SaveChanges();
            return RedirectToAction("ShowBugtracker", "Bugs", new { id = bug.BugtrackerId });
        }

        [HttpPost]
        public ActionResult AddBugFixers(int bugId, string[] users = null)
        {
            ViewBag.ProjectId = db.Projects.First(pr => pr.Bugtracker.Bugs.Any(b => b.Id == bugId)).Id;
            ViewBag.BugId = bugId;
            ViewBag.PossibleUsers = new List<ApplicationUser>();
            if (users == null)
            {
                ViewBag.AddedUsers = new List<ApplicationUser>();
            }
            else
            {
                List<ApplicationUser> addedUsers = new List<ApplicationUser>();
                foreach(string name in users)
                {
                    addedUsers.Add(db.Users.First(u => u.UserName == name));
                }
                ViewBag.AddedUsers = addedUsers;
            }
            return PartialView("AddBugFixersPartial");
        }

        [HttpPost]
        public ActionResult CreateOpenBug(int bugId, string[] users)
        {
            foreach(string userId in users)
            {
                BugFixer bugFixer = new BugFixer() { BugId = bugId, UserId = userId };
                db.BugFixers.Add(bugFixer);
                db.SaveChanges();
            }
            return RedirectToAction("MakeOpenBug", new { bugId = bugId });
        }

        [HttpPost]
        public ActionResult CreateHelpList(string inputedText, int bugId, string[] userNames = null)
        {
            int projectId = db.Projects.First(pr => pr.Bugtracker.Bugs.Any(b => b.Id == bugId)).Id;
            List<ApplicationUser> users = new List<ApplicationUser>();
            // Если пользователь ничего не ввёл, то подсказки не должно быть.
            if (inputedText != "")
            {
                // Проверка на то, был ли уже добавлен пользователь.
                users = db.Users.Where(x => (x.UserName.Contains(inputedText) || x.Name.Contains(inputedText)) &&
                    x.BugFixers.Count(bugFixer => bugFixer.BugId == bugId) == 0 &&
                    x.Participations.Any(pr => pr.ProjectId == projectId)).ToList();
            }
            if (userNames != null)
                users = users.Where(u => !userNames.Contains(u.UserName)).ToList();
            return PartialView("HelpListPartial", users);
        }



        [HttpPost]
        public ActionResult RemoveBugFixer(string[] users, string userId, int bugId)
        {
            string[] addedUsers = users.Where(user => user != userId).ToArray();
            //RouteValueDictionary dictionary = new RouteValueDictionary();
            //dictionary.Add("userId", userId);
            //dictionary.Add("users", addedUsers);
            //return RedirectToAction("AddBugFixers", dictionary);

            //!!!!!!Скопированный код, с этим надо что-то сделать!!!!!!!!
            ViewBag.ProjectId = db.Projects.First(pr => pr.Bugtracker.Bugs.Any(b => b.Id == bugId)).Id;
            ViewBag.BugId = bugId;
            ViewBag.PossibleUsers = new List<ApplicationUser>();

            if (addedUsers == null)
            {
                ViewBag.AddedUsers = new List<ApplicationUser>();
            }
            else
            {
                List<ApplicationUser> Users = new List<ApplicationUser>();
                foreach (string name in addedUsers)
                {
                    Users.Add(db.Users.First(u => u.UserName == name));
                }
                ViewBag.AddedUsers = Users;
            }
            return PartialView("AddBugFixersPartial");
        }
    }
}
