﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamBoard.Models;
using DateTimeExtensions;

namespace TeamBoard.Controllers
{
    [Authorize]
    public class ConferenceController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private int countOfMessagesPerDownload = 25;

        /// <summary>
        /// Основной метод контроллера, который возвращает выбранный чат.
        /// </summary>
        /// <param name="id">Id чата.</param>
        /// <param name="minimized">Параметр, отвечающий за то, в отдельной вкладке или в панели в канбане открыт чат.</param>
        /// <returns></returns>
        public ActionResult ShowConference(int? id, string minimized = null)
        {
            var currentUser =  db.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            if (id == null || id == -1)
            {
                var conferencesOfThisUser = db.Conferences.Where(x => x.Chattings.Any(c => c.UserId == currentUser.Id)).ToList();
                if (!String.IsNullOrEmpty(minimized))
                {
                    int projectId = Convert.ToInt32(minimized);
                    conferencesOfThisUser = conferencesOfThisUser.Where(x => x.ProjectId == projectId).ToList();
                }
                if (conferencesOfThisUser.Count() > 0)
                {
                    id = conferencesOfThisUser.First().Id;
                }
                else
                {
                    if (!String.IsNullOrEmpty(minimized))
                    {
                        return new EmptyResult();
                    }
                    return RedirectToAction("EmptyConference");
                }
            }

            Conference conference = db.Conferences.FirstOrDefault(chat => chat.Id == id);
            if (conference == null)
            {
                return RedirectToAction("AccessDenied");
            }
            var participants = db.Users.Where(user =>
                user.Chattings.Any(chatting => chatting.UserId == user.Id && chatting.ConferenceId == conference.Id));
            if (!participants.Any(user => user.Id == currentUser.Id))
            {
                return RedirectToAction("AccessDenied");
            }

            ViewBag.Conference = conference;
            // Список всех пользователей, имеющих доступ к данному чату.
            ViewBag.Participants = participants.ToList();
            ViewBag.ConnectedUser = currentUser;
            ViewBag.LastVisit = db.Chattings.First(chatting => 
                chatting.UserId == currentUser.Id && chatting.ConferenceId == conference.Id).TimeOfLastVisit;

            var orderedConferences = db.Conferences.Where(conf =>
                conf.Chattings.Any(chatting => chatting.UserId == currentUser.Id && chatting.ConferenceId == conf.Id))
                .OrderByDescending(chat => chat.Messages.Max(c => c.Time))
                .ToList();

            ViewBag.ChatList = orderedConferences
                .Select(x => new ConferenceViewModel() { 
                    Title = x.Title, 
                    Id = x.Id, 
                    Messages = x.Messages.Where(m => 
                        m.Time > currentUser.Chattings.FirstOrDefault(c => c.ConferenceId == x.Id).TimeOfLastVisit).Count() 
                }).ToList();

            // Обновим время входа.
            currentUser.Chattings.FirstOrDefault(x => x.ConferenceId == id && x.UserId == currentUser.Id)
                .TimeOfLastVisit = DateTime.Now;
            db.SaveChanges();

            if (String.IsNullOrEmpty(minimized))
            {
                // В чатах футер мы не выводим.
                ViewBag.NoFooter = true;
                return View();
            }

            ViewBag.ProjectId = conference.ProjectId;
            return PartialView("ConferencePartial");
        }

        public ActionResult AddConference(int projectId)
        {
            // Пользователи, участвующие в данном проекте.
            ViewBag.Users = db.Users.Where(user => user.Participations
                .Any(participation => participation.ProjectId == projectId)).ToList();
            ViewBag.ProjectId = projectId;
            return View();
        }

        [HttpPost]
        public ActionResult AddConference(Conference model, string[] participant)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser currentUser = db.Users.First(user => user.UserName == User.Identity.Name);
                model.CreatorId = currentUser.Id;
                db.Conferences.Add(model);

                // Добавляем текущего пользователя в чат.
                db.Chattings.Add(new Chatting()
                {
                    UserId = db.Users.First(user => user.UserName == User.Identity.Name).Id,
                    ConferenceId = model.Id,
                    TimeOfLastVisit = DateTime.Now
                });
                db.Messages.Add(new Message()
                    {
                        LabelId = 1,
                        ConferenceId = model.Id,
                        User = currentUser,
                        UserId = currentUser.Id,
                        Text = String.Format("has created chat '{0}'", 
                            model.Title),
                        Time = DateTime.Now
                    });
                db.SaveChanges();

                try
                {
                    // Добавляем остальных.
                    if (participant != null)
                    {
                        foreach (string part in participant)
                        {
                            db.Chattings.Add(new Chatting()
                            {
                                UserId = db.Users.First(user => user.Id == part).Id,
                                ConferenceId = model.Id,
                                TimeOfLastVisit = DateTime.Now
                            });
                        }
                    }
                }
                catch (NullReferenceException)
                {

                }
                db.SaveChanges();
                return RedirectToAction("ShowConference", new { id = model.Id });
            }
            ViewBag.Users = db.Users.Where(user => user.Participations
                .Any(participation => participation.ProjectId == model.ProjectId)).ToList();
            ViewBag.ProjectId = model.ProjectId;
            return View();
        }

        [HttpGet]
        public ActionResult EditConference(int conferenceId)
        {
            Conference conf = db.Conferences.First(c => c.Id == conferenceId);
            int projectId = db.Projects.First(project => project.Conferences.Any(conference => conference.Id == conferenceId)).Id;
            // Пользователи, участвующие в данном проекте.
            ViewBag.Users = db.Users.Where(user => user.Participations
                .Any(participation => participation.ProjectId == projectId) &&
                user.UserName != User.Identity.Name).ToList();
            ViewBag.ProjectId = projectId;
            return View(conf);
        }

        [HttpPost]
        public ActionResult EditConference(Conference model, string[] participant)
        {
            if (ModelState.IsValid)
            {
                db.Conferences.First(c => c.Id == model.Id).Title = model.Title;
                db.SaveChanges();

                // Удаляем всех пользователей из чата.
                List<Chatting> chattings = db.Chattings.Where(chat => chat.ConferenceId == model.Id).ToList();
                foreach(Chatting chatting in chattings)
                {
                    db.Chattings.Remove(chatting);
                    db.SaveChanges();
                }

                // Добавляем текущего пользователя в чат.
                db.Chattings.Add(new Chatting()
                {
                    UserId = db.Users.First(user => user.UserName == User.Identity.Name).Id,
                    ConferenceId = model.Id,
                    TimeOfLastVisit = DateTime.Now
                });
                db.SaveChanges();

                try
                {
                    // Добавляем остальных.
                    if (participant != null)
                    {
                        foreach (string part in participant)
                        {
                            db.Chattings.Add(new Chatting()
                            {
                                UserId = db.Users.First(user => user.Id == part).Id,
                                ConferenceId = model.Id,
                                TimeOfLastVisit = DateTime.Now
                            });
                            db.SaveChanges();
                        }
                    }
                }
                catch (NullReferenceException)
                {

                }
                return RedirectToAction("ShowConference", new { id = model.Id });
            }
            ViewBag.Users = db.Users.Where(user => user.Participations
                .Any(participation => participation.ProjectId == model.ProjectId) &&
                user.UserName != User.Identity.Name).ToList();
            ViewBag.ProjectId = model.ProjectId;
            return View(model);
        }

        [HttpPost]
        public ActionResult ShowPossibleParticipants(string[] addedUsers, int projectId, string inputedName)
        {
            List<ApplicationUser> users = db.Users.Where(user =>
                user.Participations.Count(participation => participation.ProjectId == projectId) > 0 &&
                (user.UserName.Contains(inputedName) || user.Name.Contains(inputedName)) &&
                User.Identity.Name != user.UserName).ToList();

            // Проверка на то, был ли пользователь уже добавлен ранее.
            if (addedUsers != null)
            {
                users = users.Where(user => !addedUsers.Any(u => u == user.Id)).ToList();
            }

            ViewBag.ProjectId = projectId;
            return PartialView("ShowPossibleConferenceParticipants", users);
        }

        [HttpPost]
        public ActionResult AddParticipant(string[] addedUsers, string newParticipant, int projectId, string title, int? conferenceId)
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            if (addedUsers != null)
            {
                users = db.Users.Where(user => addedUsers.Any(u => u == user.Id) || newParticipant == user.Id).ToList();
            }
            else
            {
                users = db.Users.Where(user => newParticipant == user.Id).ToList();
            }
            ViewData["AddedUsers"] = users;
            ViewData["PossibleUsers"] = new List<ApplicationUser>();
            // Если конференция уже была создана, то надо отображать другое представление.
            if (conferenceId == null)
            {
                ViewData["projectId"] = projectId;
                ViewData["Title"] = title;
                return PartialView("AddChattingPartial");
            }
            else
            {
                Conference c = db.Conferences.First(conf => conf.Id == conferenceId);
                c.Title = title;
                c.ProjectId = projectId;
                ViewData["Conference"] = c;
                return PartialView("EditChattingPartial");
            }
        }

        [HttpPost]
        public ActionResult RemoveParticipant(string[] addedUsers, string userId, int projectId, string title, int? conferenceId)
        {
            List<ApplicationUser> users = db.Users.Where(user =>
                addedUsers.Any(id => id == user.Id) && userId != user.Id).ToList();
            ViewData["AddedUsers"] = users;
            ViewData["PossibleUsers"] = new List<ApplicationUser>();
            // Если конференция уже была создана, то надо отображать другое представление.
            if (conferenceId == null)
            {
                ViewData["projectId"] = projectId;
                ViewData["Title"] = title;
                return PartialView("AddChattingPartial");
            }
            else
            {
                Conference c = db.Conferences.First(conf => conf.Id == conferenceId);
                c.Title = title;
                c.ProjectId = projectId;
                ViewData["Conference"] = c;
                return PartialView("EditChattingPartial");
            }
        }

        public ActionResult AccessDenied()
        {
            var id =  db.Users.FirstOrDefault(user => user.UserName == User.Identity.Name).Id;
            ViewBag.Conferences = db.Conferences.Where(conference =>
                conference.Chattings.Any(chatting => chatting.UserId == id))
                .Select(conf => conf.Id).ToList();
            return View();
        }
        public ActionResult EmptyConference()
        {
           return View("NoConferencesError");
        }

        [HttpPost]
        public ActionResult DownloadMessages(int id)
        {
            if (id != -1)
            {
                var lastMessage = db.Messages.First(message => message.Id == id);
                var messages = db.Messages
                    .Where(message => message.Time < lastMessage.Time && message.ConferenceId == lastMessage.ConferenceId)
                    .OrderByDescending(m => m.Time)
                    .Take(countOfMessagesPerDownload)
                    .ToList()
                    .Select(x =>
                        new { id = x.Id, author = x.User.Name, text = x.Text, userpic = x.User.Userpic.Image, self = User.Identity.Name == x.User.UserName,
                              time = Convert.ToDateTime(x.Time).Format()
                        });
                return Json(messages);
            }
            return new EmptyResult();
        }

        public ActionResult ShowList(int id)
        {

            var currentUser = db.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            ViewBag.ProjectId = id;
            var orderedConferences = db.Conferences.Where(conf =>
                conf.Chattings.Any(chatting => chatting.UserId == currentUser.Id && chatting.ConferenceId == conf.Id))
                .OrderByDescending(chat => chat.Messages.Max(c => c.Time))
                .ToList();

            ViewBag.ChatList = orderedConferences
                .Select(x => new ConferenceViewModel()
                {
                    Title = x.Title,
                    Id = x.Id,
                    Messages = x.Messages.Where(m =>
                        m.Time > currentUser.Chattings.FirstOrDefault(c => c.ConferenceId == x.Id).TimeOfLastVisit).Count()
                }).ToList();
            return PartialView("ShowList");
        }
    }
}