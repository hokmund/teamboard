﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TeamBoard.Models;

namespace TeamBoard.Controllers
{
    [Authorize]
    public class ProjectsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public ActionResult Index()
        {
            // Отображаем только проекты, в которых данный пользователь участвует или является автором.
            var projects = db.Projects.Where(project =>
                    project.Participations
                        .Any(participant => participant.User.UserName == User.Identity.Name)
                    || project.Author.UserName == User.Identity.Name).ToList();
            return View(projects);
        }

        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.FirstOrDefault(x=> x.Id == id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Project project)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser currentUser = db.Users.First(user => user.UserName == User.Identity.Name);
                project.AuthorId = currentUser.Id;
                project.Date = DateTime.Now;
                // Добавляем текущего пользователя в участников проекта.
                db.Participations.Add(new Participation { ProjectId = project.Id, UserId = currentUser.Id });

                db.Projects.Add(project);
                db.SaveChanges();

                var currentProject = db.Projects.First(pr => pr.Id == project.Id);

                // Добавляем багтрекер и канбан, привязанные к данному проекту.
                db.Bugtrackers.Add(new Bugtracker { Id = project.Id,  Project = currentProject});
                db.Kanbans.Add(new Kanban { Project = currentProject, Id = currentProject.Id });


                db.SaveChanges();

                foreach (var style in Project.DefaultLabelStyles)
                {
                    db.Labels.Add(new Label
                        {
                            Style = style,
                            Header = "",
                            Project = project
                        });
                }

                db.SaveChanges();

                return RedirectToAction("Details", new { id = currentProject.Id });
            }

            return View(project);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProjectName = project.Name;

            // Создаём новый список участников.
            List<ApplicationUser> addedUsers = new List<ApplicationUser>();
            Project pr = db.Projects.First(p => p.Id == id);
            foreach (Participation participation in pr.Participations)
            {
                if (participation.User.UserName != User.Identity.Name)
                    addedUsers.Add(participation.User);
            }

            //List<ApplicationUser> addedTesters = db.Users.Where(user => user.Participations.Any(p => p.IsQA)).ToList();

            //ViewBag.AddedTesters = addedTesters;
            ViewBag.AddedUsers = addedUsers;
            return View(project);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Project project)
        {
            if (ModelState.IsValid)
            {
                db.Projects.First(p => p.Id == project.Id).Name = project.Name;
                db.Projects.First(p => p.Id == project.Id).Description = project.Description;
                db.SaveChanges();
                var idDictionary = new RouteValueDictionary();
                idDictionary.Add("id", project.Id);
                return RedirectToAction("Details", idDictionary);
            }

            // Создаём новый список участников.
            List<ApplicationUser> addedUsers = new List<ApplicationUser>();
            Project pr = db.Projects.First(p => p.Id == project.Id);
            foreach (Participation participation in pr.Participations)
            {
                addedUsers.Add(participation.User);
            }
            List<ApplicationUser> addedTesters = db.Users.Where(user => user.Participations.Any(p => p.IsQA)).ToList();

            ViewBag.ProjectName = project.Name;
            //ViewBag.AddedTesters = addedTesters;
            ViewBag.AddedUsers = addedUsers;
            return View(project);
        }

        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project project = db.Projects.Find(id);
            db.Kanbans.Remove(project.Kanban);
            db.Bugtrackers.Remove(project.Bugtracker);
            db.Projects.Remove(project);
            var labels = db.Labels.Where(l => l.ProjectId == project.Id);
            db.Labels.RemoveRange(labels);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult RemoveParticipant(string userId, int projectId)
        {
            //Удаляем одного участника.
            db.Participations.Remove(db.Participations.First(p => p.UserId == userId && p.ProjectId == projectId));
            db.SaveChanges();

            // Создаём новый список участников.
            List<ApplicationUser> addedUsers = new List<ApplicationUser>();
            Project project = db.Projects.First(pr => pr.Id == projectId);
            foreach (Participation participation in project.Participations)
            {
                if (participation.User.UserName != User.Identity.Name)
                    addedUsers.Add(participation.User);
            }

            ViewData["AddedUsers"] = addedUsers;
            ViewData["projectId"] = projectId;
            return PartialView("AddedUsersList");
        }

        [HttpPost]
        public ActionResult AddParticipant(int projectId, string inputedName)
        {
            //Создаём одного участника.
            if (db.Users.Count(u => inputedName == u.UserName) > 0)
            {
                ApplicationUser user = db.Users.First(u => inputedName == u.UserName);
                db.Participations.Add(new Participation() { UserId = user.Id, ProjectId = projectId, IsQA = false });
                db.SaveChanges();
            }
            // Создаём новый список участников.
            List<ApplicationUser> addedUsers = new List<ApplicationUser>();
            Project project = db.Projects.First(pr => pr.Id == projectId);
            foreach (Participation participation in project.Participations)
            {
                if (participation.User.UserName != User.Identity.Name)
                    addedUsers.Add(participation.User);
            }

            ViewData["AddedUsers"] = addedUsers;
            ViewData["PossibleUsers"] = new List<ApplicationUser>();
            ViewData["projectId"] = projectId;
            return PartialView("AddParticipantsPartial");
        }

        [HttpPost]
        public ActionResult CreateHelpList(string inputedText, int projectId)
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            // Если пользователь ничего не ввёл, то подсказки не должно быть.
            if (inputedText != "")
            {
                // Проверка на то, был ли уже добавлен пользователь.
                users = db.Users.Where(x => (x.UserName.Contains(inputedText) || x.Name.Contains(inputedText)) &&
                    x.Participations.Count(participation => participation.ProjectId == projectId) == 0 &&
                    x.UserName != User.Identity.Name).ToList();
            }
            return PartialView("HelpListPartial", users);
        }

        [HttpPost]
        public ActionResult CreateTestersList(string inputedTesterText, int projectId)
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            // Если пользователь ничего не ввёл, то подсказки не должно быть.
            if (inputedTesterText != "")
            {
                // Проверка на то, является ли пользователь уже тестировщиком.
                users = db.Users.Where(x => (x.UserName.Contains(inputedTesterText) || x.Name.Contains(inputedTesterText)) &&
                    x.Participations.Any(participation => participation.ProjectId == projectId && !participation.IsQA)).ToList();
            }
            return PartialView("TestersHelpListPartial", users);
        }

        [HttpPost]
        public ActionResult AddTester(int projectId, string inputedTesterName)
        {
            db.Participations.First(participation => participation.ProjectId == projectId && participation.User.UserName == inputedTesterName).IsQA = true;
            db.SaveChanges();

            ViewData["AddedTesters"] = db.Users.Where(user => user.Participations.Any(participation => participation.ProjectId == projectId && participation.IsQA)).ToList();
            ViewData["PossibleTesters"] = new List<ApplicationUser>();
            ViewData["projectId"] = projectId;
            return PartialView("AddTestersPartial");
        }

        [HttpPost]
        public ActionResult RemoveTester(int projectId, string userId)
        {
            db.Participations.First(participation => participation.ProjectId == projectId && participation.User.Id == userId).IsQA = false;
            db.SaveChanges();

            ViewData["AddedTesters"] = db.Users.Where(user => user.Participations.Any(participation => participation.ProjectId == projectId && participation.IsQA)).ToList();
            ViewData["PossibleTesters"] = new List<ApplicationUser>();
            ViewData["projectId"] = projectId;
            return PartialView("AddedTestersList");
        }
    }
}
