﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamBoard.Models;
using System.Data.Entity;

namespace TeamBoard.Controllers
{
    public class KanbanController : Controller
    {
        ApplicationDbContext dbContext = new ApplicationDbContext();


        [ChildActionOnly]
        [Authorize]
        public ActionResult Index(int id)
        {
            //dbContext.Kanbans.Include(k => k.Partitions);
            var project = dbContext.Projects.FirstOrDefault(p => p.Id == id);
            if (project != null)
            {
                if (!isUserHaveAccess(project.Kanban))
                {
                    return new EmptyResult();
                }

                dbContext.Kanbans.Include(k => k.Partitions);
                return PartialView(project.Kanban);
            }
            else
            {
                throw new KeyNotFoundException(String.Format("Project with Id={0} wasn't found", id));
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddColumn(int id)
        {
            var kanban = dbContext.Kanbans.FirstOrDefault(k => k.Id == id);

            if (!isUserHaveAccess(kanban))
            {
                return new EmptyResult();
            }
            if (kanban != null)
            {
                var partition = new Partition
                    {
                        Kanban = kanban,
                        Header = "Division",
                        Position = dbContext.Partitions.Where(p => p.KanbanId == kanban.Id).Count()
                    };

                dbContext.Partitions.Add(partition);
                dbContext.SaveChanges();

                var subpartition = new Subpartition
                {
                    Partition = partition,
                    Header = "Subdivision",
                    Position = 0
                };

                dbContext.Subpartitions.Add(subpartition);
                dbContext.SaveChanges();

                //var result = new JsonResult();
                //result.Data = "OK";
                return Json(new 
                { 
                    id = partition.Id, 
                    subId = subpartition.Id, 
                    position = partition.Position 
                });
            }
            else
            {
                throw new KeyNotFoundException(String.Format("Kanban with Id={0} wasn't found", id));
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult DeleteColumn(int id)
        {
            if (!isUserHaveAccess(dbContext.Partitions
                .FirstOrDefault(p => 
                    p.Id == id).Kanban))
            {
                return new EmptyResult();
            }

            dbContext.Partitions.RemoveRange(dbContext.Partitions.Where(p => p.Id == id));
            dbContext.SaveChanges();

            int position = 0;
            foreach (var partition in dbContext.Partitions.OrderBy(p => p.Position))
            {
                partition.Position = position;
                position += 1;
            }
            dbContext.SaveChanges();
            
            var result = new JsonResult();
            result.Data = "OK";
            return result;
        }

        [HttpPost]
        [Authorize]
        public ActionResult RenameColumn(int id, string header)
        {
            var partition = dbContext.Partitions.FirstOrDefault(p => p.Id == id);

            header = header.Trim();
            if (partition != null && partition.Header != header)
            {
                if (!isUserHaveAccess(partition.Kanban))
                {
                    return new EmptyResult();
                }

                partition.Header = header;
                dbContext.SaveChanges();

                var result = new JsonResult();
                result.Data = "OK";
                return result;
            }
            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult ReplaceColumn(int id, int newPosition)
        {
            //Get column with this id
            var replaced = dbContext.Partitions.FirstOrDefault(p => p.Id == id);

            //If column exists
            if (replaced != null)
            {
                // Check user access
                if (!isUserHaveAccess(replaced.Kanban))
                {
                    return new EmptyResult();
                }

                //Get replacing direction
                int direction = (replaced.Position - newPosition) > 0 ? 1 : -1;

                //Set column position
                replaced.Position = newPosition;
                dbContext.SaveChanges();

                

                //Select other columns from the same kanban
                var columns = dbContext.Partitions
                    .Where(p => p.KanbanId == replaced.KanbanId && p.Id != replaced.Id)
                    .ToList();

                //Set new position for columns
                foreach (var c in columns)
                {
                    if (c.Position < replaced.Position)
                    {
                        c.Position -= 1;
                    }
                    else if (c.Position > replaced.Position)
                    {
                        c.Position += 1;
                    }
                    else
                    {
                        c.Position += direction;
                    }
                }
                dbContext.SaveChanges();

                //Get all kanban columns
                var forNormalize = dbContext.Partitions
                    .Where(p => p.KanbanId == replaced.KanbanId)
                    .OrderBy(p => p.Position)
                    .ToList();

                //Normalize columns
                int position = 0;
                foreach (var column in forNormalize)
                {
                    column.Position = position;
                    position += 1;
                }
                dbContext.SaveChanges();
            }
            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddSticker(int subpartitionId, string header)
        {
            var subpartition = dbContext.Subpartitions.FirstOrDefault(sp => sp.Id == subpartitionId);
            if (subpartition != null)
            {
                if (!isUserHaveAccess(subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                Sticker sticker = new Sticker
                {
                    //Label = text,
                    User = dbContext.Users.FirstOrDefault(u => u.UserName == User.Identity.Name),
                    Subpartition = subpartition,
                    Header = header,
                    Position = subpartition.Stickers.Count
                };
                dbContext.Stickers.Add(sticker);
                dbContext.SaveChanges();

                var result = new JsonResult();
                result.Data = "OK";
                return result;
            }


            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult DeleteSticker(int id)
        {
            var sticker = dbContext.Stickers
                .FirstOrDefault(s => s.Id == id);

            if (sticker != null)
            {
                if (!isUserHaveAccess(sticker.Subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                int subpartitionId = sticker.SubpartitionId;
                dbContext.Stickers.Remove(sticker);
                dbContext.SaveChanges();

                int newPosition = 0;
                foreach (var newSticker in dbContext.Stickers.Where(s => s.SubpartitionId == subpartitionId).OrderBy(s => s.Position))
                {
                    newSticker.Position = newPosition;
                    newPosition += 1;
                }
                dbContext.SaveChanges();

                var result = new JsonResult();
                result.Data = "OK";
                return result;
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult RenameSticker(int id, string header)
        {
            // Get renaming sticker
            var sticker = dbContext.Stickers.FirstOrDefault(s => s.Id == id);
            // If sticker exists
            if (sticker != null)
            {
                if (!isUserHaveAccess(sticker.Subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                // Set new header to sticker
                sticker.Header = header;
                dbContext.SaveChanges();
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult GetStickerInfo(int subpartitionId, int position)
        {
            var sticker = dbContext.Stickers
                .FirstOrDefault(s => s.SubpartitionId == subpartitionId && s.Position == position);

            if (sticker != null)
            {
                if (!isUserHaveAccess(sticker.Subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                return Json(
                    new
                    {
                        Header = sticker.Header,
                        Description = sticker.Description,
                        Id = sticker.Id,
                        SubpartitionId = sticker.SubpartitionId,
                        IsTasked = sticker.TaskExecutions
                            .Any(te => te.User.UserName == User.Identity.Name),
                        Executors = sticker.TaskExecutions
                            .Where(t => t.User.UserName != User.Identity.Name)
                            .Select(t => t.User.Name),
                        Label = sticker.Label != null ?
                                new { Id = sticker.LabelId, Style = sticker.Label.Style, Header = sticker.Label.Header }
                                : null,
                        AllLabels = sticker.Subpartition.Partition.Kanban.Project.Labels
                                    .Select(l => new
                                    {
                                        Id = l.Id,
                                        Style = l.Style,
                                        Header = l.Header
                                    }
                        )
                    });
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult SetStickerDescription(int id, string description)
        {
            var sticker = dbContext.Stickers.FirstOrDefault(s => s.Id == id);
            if (sticker != null)
            {
                if (!isUserHaveAccess(sticker.Subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                sticker.Description = string.IsNullOrEmpty(description) ? null : description;
                dbContext.SaveChanges();

                var result = new JsonResult();
                return result;
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult SetStickerLabel(int stickerId, int? labelId)
        {
            // Get sticker for label
            var sticker = dbContext.Stickers.FirstOrDefault(s => s.Id == stickerId);
            // If sticker exists
            if (sticker != null)
            {
                if (!isUserHaveAccess(sticker.Subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                // If labelId is empty
                if (labelId == null)
                {
                    // Clear sticker label
                    sticker.Label = null;
                    sticker.LabelId = null;
                }
                // Else
                else
                {
                    //Get label
                    var label = dbContext.Labels.FirstOrDefault(l => l.Id == labelId.Value);
                    //If label exists
                    if (label != null)
                    {
                        // Set sticker label
                        sticker.LabelId = label.Id;
                    }
                    
                }

                dbContext.SaveChanges();
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult SetStickerTaskExecuting(int id, bool isExecuting)
        {
            // Get sticker with this id
            var sticker = dbContext.Stickers.FirstOrDefault(s => s.Id == id);

            // If sticker exists
            if (sticker != null)
            {
                if (!isUserHaveAccess(sticker.Subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                // Get current user
                var currentUser = dbContext.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);

                //If user exists
                if (currentUser != null)
                {
                    // Find task with current user and sticker
                    var task = dbContext.TaskExecutions
                        .FirstOrDefault(t => t.StickerId == id && t.UserId == currentUser.Id);

                    // If set executing
                    if (isExecuting)
                    {
                        // If task doesn't exist
                        if (task == null)
                        {
                            //Create new task
                            TaskExecution newTask = new TaskExecution
                            {
                                Sticker = sticker,
                                User = currentUser
                            };
                            dbContext.TaskExecutions.Add(newTask);
                            dbContext.SaveChanges();
                        }
                    }
                    //Else
                    else
                    {
                        // If task exists
                        if (task != null)
                        {
                            // Remove task
                            dbContext.TaskExecutions.Remove(task);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult ReplaceSticker(
            int fromSubpartitionId, int oldPosition, 
            int toSubpartitionId, int newPosition)
        {
            // Get replacing sticker
            var replacingSticker = dbContext.Stickers.FirstOrDefault(s => s.SubpartitionId == fromSubpartitionId
                && s.Position == oldPosition);
            // If sticker exists
            if (replacingSticker != null)
            {
                if (!isUserHaveAccess(replacingSticker.Subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                // If "from" subpartion equals to "to" subpartition
                if (fromSubpartitionId == toSubpartitionId)
                {
                    // Set new sticker position
                    replacingSticker.Position = newPosition;
                    dbContext.SaveChanges();

                    // Calc replacing direction
                    int direction = (oldPosition - newPosition) > 0 ? 1 : -1;

                    // Get all stickers from subpartition except replacing
                    var subpartitionStickers = dbContext.Stickers
                        .Where(s => s.SubpartitionId == replacingSticker.SubpartitionId
                                    && s.Id != replacingSticker.Id)
                        .ToList();

                    // Update position of other stickers in this subpartition
                    foreach (var s in subpartitionStickers)
                    {
                        if (s.Position < replacingSticker.Position)
                        {
                            s.Position -= 1;
                        }
                        else if (s.Position > replacingSticker.Position)
                        {
                            s.Position += 1;
                        }
                        else
                        {
                            s.Position += direction;
                        }
                    }
                    dbContext.SaveChanges();

                    // Normalize stickers positions (guarantee that positions will be consecutive) 
                    var forNormalize = dbContext.Stickers
                        .Where(s => s.SubpartitionId == replacingSticker.SubpartitionId)
                        .OrderBy(s => s.Position)
                        .ToList();
                    int position = 0;
                    foreach (var sticker in forNormalize)
                    {
                        sticker.Position = position;
                        position += 1;
                    }
                    dbContext.SaveChanges();
                }
                // Else
                else
                {
                    // Set new sticker position and subpartition
                    replacingSticker.Position = newPosition;
                    replacingSticker.SubpartitionId = toSubpartitionId;
                    dbContext.SaveChanges();

                    // Get stickers in "from" subpartition
                    var fromStickers = dbContext.Stickers
                        .Where(s => s.SubpartitionId == fromSubpartitionId)
                        .OrderBy(s => s.Position)
                        .ToList();

                    // Update stickers position in "from" subpartition
                    int fromPosition = 0;
                    foreach (var sticker in fromStickers)
                    {
                        sticker.Position = fromPosition;
                        fromPosition += 1;
                    }

                    // Get stickers in "to" subpartition except replacing
                    var toStickers = dbContext.Stickers
                        .Where(s => s.SubpartitionId == toSubpartitionId && s.Id != replacingSticker.Id)
                        .ToList();

                    // Update stickers position in "to" subpartition
                    foreach (var sticker in toStickers)
                    {
                        if (sticker.Position < newPosition)
                        {
                            sticker.Position -= 1;
                        }
                        else
                        {
                            sticker.Position += 1;
                        }
                    }
                    dbContext.SaveChanges();

                    // Normalize stickers positions (guarantee that positions will be consecutive) 
                    var stickers = dbContext.Stickers
                        .Where(s => s.SubpartitionId == replacingSticker.SubpartitionId)
                        .OrderBy(s => s.Position)
                        .ToList();
                    int toPosition = 0;
                    foreach (var sticker in stickers)
                    {
                        sticker.Position = toPosition;
                        toPosition += 1;
                    }
                    dbContext.SaveChanges();
                }
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddSubpartition(int columnId)
        {
            var column = dbContext.Partitions.FirstOrDefault(p => p.Id == columnId);
            if (column != null)
            {
                if (!isUserHaveAccess(column.Kanban))
                {
                    return new EmptyResult();
                }

                Subpartition subpartition = new Subpartition
                {
                    Header = "Subdivision",
                    Partition = column,
                    Position = column.Subpartitions.Count(),
                };
                dbContext.Subpartitions.Add(subpartition);
                dbContext.SaveChanges();

                return Json(new 
                { 
                    id = subpartition.Id,
                    columnPosition = column.Position,
                    subpartitionPosition = subpartition.Position,
                });
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult DeleteSubpartition(int subpartitionId)
        {
            var subpartition = dbContext.Subpartitions.FirstOrDefault(sp => sp.Id == subpartitionId);
            if (subpartition != null)
            {
                if (!isUserHaveAccess(subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                dbContext.Subpartitions.Remove(subpartition);
                dbContext.SaveChanges();

                return Json(new { status = "OK" });
            }

            return Json(new { status = "ERROR" });
        }

        [HttpPost]
        [Authorize]
        public ActionResult RenameSubpartition(int subpartitionId, string header)
        {
            var subpartition = dbContext.Subpartitions.FirstOrDefault(sp => sp.Id == subpartitionId);
            if (subpartition != null)
            {
                if (!isUserHaveAccess(subpartition.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                subpartition.Header = header;
                dbContext.SaveChanges();

                return Json(new { status = "OK" });
            }

            return Json(new { status = "ERROR" });
        }

        [HttpPost]
        [Authorize]
        public ActionResult ReplaceSubpartition(int id, int newPosition, int columnId)
        {
            //Get subcolumn with this id
            var replaced = dbContext.Subpartitions.FirstOrDefault(p => p.Id == id);

            //If subcolumn exists
            if (replaced != null)
            {
                if (!isUserHaveAccess(replaced.Partition.Kanban))
                {
                    return new EmptyResult();
                }

                //Get replacing direction
                int direction = (replaced.Position - newPosition) > 0 ? 1 : -1;

                //Set subcolumn position
                replaced.Position = newPosition;
                replaced.PartitionId = columnId;
                dbContext.SaveChanges();



                //Select other subcolumns from the same column
                var subcolumns = dbContext.Subpartitions
                    .Where(p => p.PartitionId == replaced.PartitionId && p.Id != replaced.Id)
                    .ToList();

                //Set new position for subcolumns
                foreach (var s in subcolumns)
                {
                    if (s.Position < replaced.Position)
                    {
                        s.Position -= 1;
                    }
                    else if (s.Position > replaced.Position)
                    {
                        s.Position += 1;
                    }
                    else
                    {
                        s.Position += direction;
                    }
                }
                dbContext.SaveChanges();

                //Get all column subcolumns
                var forNormalize = dbContext.Subpartitions
                    .Where(p => p.PartitionId == replaced.PartitionId)
                    .OrderBy(p => p.Position)
                    .ToList();

                //Normalize subcolumns position
                int position = 0;
                foreach (var subcolumn in forNormalize)
                {
                    subcolumn.Position = position;
                    position += 1;
                }
                dbContext.SaveChanges();
            }

            return new EmptyResult();
        }

        [NonAction]
        private bool isUserHaveAccess(Kanban kanban)
        {
            // Get current user
            var user = dbContext.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            // If user and kanban exist
            if (user != null && kanban != null)
            {
                // If user in kanban participants list
                if (kanban.Project.Participations.Any(p => p.UserId == user.Id))
                {
                    // User has access
                    return true;
                }
                // Else
                else
                {
                    // User has no access
                    return false;
                }
            }

            return false;
        }
    }
}