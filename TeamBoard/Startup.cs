﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TeamBoard.Startup))]
namespace TeamBoard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
