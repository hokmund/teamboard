﻿function addColumnButtonClick(e) {
    // Create new column
    var element = $("<div></div>",
        {
            class: "kanban-column"
        });

    // Fill column with inner content
    element.append($("<div>", { class: "kanban-column-header" })
                .append($("<h3>", { text: "Division" }).dblclick(editColumnHeader))
                .append($("<i>", { class: "fa fa-close fa-lg kanban-column-close", id: "newColumn" }).click(deleteColumn)))
           .append($("<div>", { class: "btn btn-default subpartition-add-btn", text: "Add subdivision" }).click(addSubpartition))
           .append($("<div>", { class: "subpartition" })
               .append($("<div>", { class: "sticker-add-btn sticker-hidden-btn" })
                    .append($("<div>", { class: "btn btn-default" }).click(addSticker)
                        .append($("<i>", { class: "fa fa-plus" }))
                        .append("Add sticker")))
               .append($("<input>", { type: "hidden", class: "subpartition-id", value: undefined, id: "newSubpartitionId" })))
           .append($("<input>", { type: "hidden", class: "partition-id", id: "newColumnId" }));

    lastColumn = element;

    // Add column to kanban
    element.insertBefore("#addColumnButton")

    var kanbanId = $("#id")[0].value;

    $.ajax({
        url: ADD_COLUMN_ROUTE,
        context: document.body,
        method: "POST",
        success: columnAdded,
        data: { id: kanbanId }
    });

    updateSortables();
}

function columnAdded(data) {
    var columnIdElement = $($(".partition-id")[data.position]);

    // Set id of added column
    $(".kanban-column-close")[data.position].id = data.id;
    columnIdElement.val(data.id);
    columnIdElement.id = "";

    var column = columnIdElement.parent();
    column.find(".subpartition-id").val(data.subId);
}

function deleteColumn(e) {
    // Get deleting column
    var deleting = $(e.currentTarget).parents(".kanban-column");

    // Get id of deleting column
    var id = deleting.children(".partition-id")[0].value;

    deleting.remove();

    $.ajax({
        url: DELETE_COLUMN_ROUTE,
        context: document.body,
        method: "POST",
        data: { id: id }
    });
}

function replaceColumn(e, ui) {
    var column = $(ui.item);

    var id = column.children(".partition-id")[0].value;
    var index = column.index();

    $.ajax(
        {
            url: REPLACE_COLUMN_ROUTE,
            data: { id: id, newPosition: index },
            context: document.body,
            method: "POST"
        });
}

function addSubpartition(e) {
    // Create new subpartition
    var sub = $("<div>", { class: "subpartition ui-sortable" })
        .append($("<div>", { class: "subpartition-header" }).dblclick(editSubpartitionHeader)
            .append($("<h4>", { text: "Subdivision" } ))
            .append($("<i>", { class: "fa fa-remove subpartition-remove-btn" }).click(removeSubpartition)))
        .append($("<div>", { class: "sticker-add-btn sticker-hidden-btn" })
            .append($("<div>", { class: "btn btn-default" }).click(addSticker)
                .append($("<i>", { class: "fa fa-plus" }))
                .append("Add sticker")))
        .append($("<input>", { type: "hidden", class: "subpartition-id", value: undefined, id: "newSubpartitionId" }));

    var column = $(e.currentTarget).parent();

    // If it is first added subpartition
    if (column.children(".subpartition").length == 1) {
        // Set header to existing subpartition
        column.children(".subpartition")
            .prepend($("<div>", { class: "subpartition-header" }).dblclick(editSubpartitionHeader)
                .append($("<h4>", { text: "Subdivision" }))
                .append($("<i>", { class: "fa fa-remove subpartition-remove-btn" }).click(removeSubpartition)));
    }

    var lastSubpartition = column.children(".subpartition").last();

    sub.insertAfter(lastSubpartition);

    var id = column.children(".partition-id")[0].value;

    $.ajax(
        {
            url: ADD_SUBPARTITION_ROUTE,
            method: "POST",
            context: document.body,
            data: { columnId: id },
            success: subpartitionAdded
        });

    updateSortables();
}

function subpartitionAdded(data) {
    var column = $(".kanban-column")[data.columnPosition];
    var subpartition = $(column).children(".subpartition")[data.subpartitionPosition];

    $(subpartition).children(".subpartition-id")[0].value = data.id;
}

function editSubpartitionHeader(e) {
    var subpartitionHeader = $(e.currentTarget);

    // Cancel editing, if name editor for this subpartition already exists
    if (subpartitionHeader.children(".name-editor").length != 0) {
        return;
    }

    // Create new editor for subpartition header
    var editor = $("<input>",
        {
            type: "textbox",
            value: subpartitionHeader.children("h4").text().trim(),
            class: "name-editor",
        })
        .focusout(saveSubpartitionHeader)
        .keydown(function (e) {
            var code = e.which;
            if (code == 13) {
                saveSubpartitionHeader(e)
            }
        })

    // Remove existing header content from subpartition
    subpartitionHeader.children().remove();

    // Add editor to subpartition
    subpartitionHeader.prepend(editor);

    editor.focus().select();
}

function saveSubpartitionHeader(e) {
    var editorInput = $(e.currentTarget);
    var headerText = editorInput.val().trim();

    if (headerText.length != 0) {
        // Get subpartition id
        var id = editorInput.parents(".subpartition").children(".subpartition-id").val();

        if ($.isNumeric(id)) {

            // Replace name editor with default header
            editorInput.parent()
                    .append($("<h4>", { text: headerText }))
                    .append($("<i>", { class: "fa fa-remove fa-lg subpartition-remove-btn" })
                        .click(removeSubpartition));
            editorInput.remove();


            $.ajax({
                url: RENAME_SUBPARTITION_ROUTE,
                context: document.body,
                method: "POST",
                data: { subpartitionId: id, header: headerText }
            });
        }

    }

}

function removeSubpartition(e) {
    var subpartition = $(e.currentTarget).parent().parent();

    // Get subpartition id
    var id = subpartition.children(".subpartition-id")[0].value;

    var column = subpartition.parent();
    subpartition.remove();

    // Hide subpartition header if only one subpartition left
    if (column.children(".subpartition").length == 1) {
        column.children(".subpartition").children(".subpartition-header").hide();
    }

    $.ajax(
    {
        url: DELETE_SUBPARTITION_ROUTE,
        method: "POST",
        context: document.body,
        data: { subpartitionId: id }
    });
}

function replaceSubcolumn(e, ui) {
    var subpartition = $(ui.item);

    // Get subpartition id
    var id = subpartition.children(".subpartition-id")[0].value;

    var column = subpartition.parent();

    $.ajax(
        {
            url: REPLACE_SUBCOLUMN_ROUTE,
            data: {
                id: id,
                newPosition: column.children(".subpartition").index(subpartition),
                columnId: column.children(".partition-id").val()
            },
            context: document.body,
            method: "POST"
        });

    var sourceColumn = ui.sender;

    // If source and target columns don't match
    if (sourceColumn != null) {
        // If only one subpartition left in source column
        if (sourceColumn.children(".subpartition").length == 1) {
            sourceColumn.find(".subpartition-header").hide();
        }

        var targetSubcolumns = column.children(".subpartition").not(subpartition);
        // If it was one subpartition before moving in target column
        if (targetSubcolumns.length == 1) {
            var emptyHeadered = targetSubcolumns.first();

            // If header doesn't exist
            if (emptyHeadered.children(".subpartition-header").length == 0) {
                emptyHeadered
                    .prepend($("<div>", { class: "subpartition-header" }).dblclick(editSubpartitionHeader)
                        .append($("<h4>", { text: "Subdivision" }))
                        .append($("<i>", { class: "fa fa-remove subpartition-remove-btn" }).click(removeSubpartition)));
            }
            else {
                emptyHeadered.children(".subpartition-header").show();
            }
        }
    }
}

function addSticker(e) {
    // Create new sticker editor
    var stickerEditorContainer =
        $("<div>")
            .append($("<div>", { class: "sticker-editor" })
                .append($("<textarea>", { class: "sticker-header-editor" }).focus())
                .append($("<div>", { class: "btn btn-default", text: "Cancel", style: "margin-left: 20px;" })
                    .click(cancelSticker))
                .append($("<div>", { class: "btn btn-primary", text: "Add", style: "margin-left: 10px;" })
                    .click(saveSticker)));

    var addStickerButtonContainer = $(e.currentTarget).parent();

    // Add sticker editor to subpartition
    stickerEditorContainer.insertBefore(addStickerButtonContainer);

    addStickerButtonContainer.hide();

    // Guaratees focus on sticker editor textarea
    setTimeout(function () {
        stickerEditorContainer.find("textarea").focus();
    }, 0);
}

function saveSticker(e) {
    var stickerEditor = $(e.currentTarget).parent();

    // Get entered sticker header text
    var text = stickerEditor.children(".sticker-header-editor").val().trim();

    if (text.length != 0) {
        var sticker = stickerEditor.parent();
        var subpartition = sticker.parent();
        
        sticker
            .addClass("sticker")
            .append($("<h4>", { text: text }))
            .click(showInfo);

        subpartition.children(".sticker-add-btn").show();

        stickerEditor.remove();

        // Get subpartition id
        var id = subpartition.children(".subpartition-id").val();

        $.ajax({
            url: ADD_STICKER_ROUTE,
            context: document.body,
            data: { subpartitionId: id, header: text },
            method: "POST",
        });

        // Prevents info dialog opening 
        e.stopPropagation();
    }
}

function cancelSticker(e) {
    var sticker = $(e.currentTarget).parent().parent();
    var subpartition = sticker.parent();

    subpartition.children(".sticker-add-btn").show();

    sticker.remove();
}

var fromSubcolumnId;
var fromSubcolumnPosition;
function replaceSticker(e, ui) {
    if (fromSubcolumnPosition == undefined) {
        return;
    }

    var sticker = $(ui.item);
    var subcolumn = sticker.parent();

    // Get resulting subcolumn id
    var toSubcolumnId = subcolumn.children(".subpartition-id").val();

    var newPosition = subcolumn.children(".sticker").index(sticker);

    if (fromSubcolumnId == undefined) {
        fromSubcolumnId = toSubcolumnId;
    }

    $.ajax(
        {
            url: REPLACE_STICKER_ROUTE,
            data: {
                fromSubpartitionId: fromSubcolumnId,
                oldPosition: fromSubcolumnPosition,
                toSubpartitionId: toSubcolumnId,
                newPosition: newPosition,
            },
            context: document.body,
            method: "POST"
        });

    // If sticker was moved in subpartition
    if (fromSubcolumnId == toSubcolumnId) {
        // If it is only one sticker in subpartition
        if (subcolumn.children(".sticker").length == 1) {
            // Replace sticker in the beginning of subpartition
            sticker.insertBefore($(subcolumn.children(".sticker-add-btn")));
        }
    }

    fromSubcolumnId = undefined;
    fromSubcolumnPosition = undefined;
}

function stickerSortStart(e, ui) {
    var sticker = $(ui.item);
    var subcolumn = sticker.parent();

    fromSubcolumnPosition = subcolumn.children(".sticker").index(sticker);
}

function stickerOut(e, ui) {
    var subcolumn = $(ui.item).parent();

    fromSubcolumnId = subcolumn.children(".subpartition-id").val();
}

function editColumnHeader(e) {
    var columnHeader = $(e.currentTarget);

    // Create column header editor
    var editor = $("<input>",
        {
            type: "textbox",
            value: columnHeader.text().trim(),
            class: "name-editor",
        })
        .focusout(saveColumnHeader)
        .keydown(function (e) {
            var code = e.which;

            // If "Enter" key pressed
            if (code == 13) {
                saveColumnHeader(e);
            }
        });

    columnHeader.replaceWith(editor);

    editor.focus().select();
}

function saveColumnHeader(e) {
    var headerInput = $(e.currentTarget);

    var headerText = headerInput.val().trim();

    if (headerText.length != 0) {
        var column = headerInput.parent().parent();

        // Get column id
        var id = column.children("input.partition-id").val();

        if ($.isNumeric(id)) {
            // Replace header editor with its text
            headerInput.replaceWith(
                $("<h3>", { text: headerText })
                    .dblclick(editColumnHeader));
            
            $.ajax({
                url: RENAME_COLUMN_ROUTE,
                context: document.body,
                method: "POST",
                data: { id: id, header: headerText }
            });
        }                
    }
}

var isEmptyDescription;
var stickerInfo;
var shownSticker;

function showInfo(e) {
    shownSticker = $(e.currentTarget);
    var subpartition = shownSticker.parent();

    var id = subpartition.children(".subpartition-id").val();
    var position = subpartition.children(".sticker").index(shownSticker);

    $.ajax({
        url: GET_STICKER_INFO_ROUTE,
        context: document.body,
        data: { subpartitionId: id, position: position },
        method: "POST",
        complete: infoGettingCompleted
    });
}

function infoGettingCompleted(data, status) {
    // Set sticker header text
    $("#stickerInfoHeader").text(data.responseJSON.Header);

    // Set sticker description
    if (data.responseJSON.Description == null) {
        $("#stickerInfoDescription").text("There is no description.");
        isEmptyDescription = true;
    }
    else {
        $("#stickerInfoDescription").text(data.responseJSON.Description);
        isEmptyDescription = false;
    }

    // Save sticker id
    $("#stickerId").val(data.responseJSON.Id);

    // Clear all executors of last opened sticker
    $("#executors").children().remove();

    if (data.responseJSON.IsTasked) {
        setExecuting($("#executeBtn"));
    }
    else {
        setNonExecuting($("#executeBtn"));
    }

    // Set sticker executors information
    for (var i = 0; i < data.responseJSON.Executors.length; i++) {
        $("#executors").append($("<div>", { text: data.responseJSON.Executors[i] }));
    }

    var labelsContainer = $("#labels");

    // Clear all labels of last opened sticker
    labelsContainer.children().remove();

    // Create empty label and add it to labels list
    labelsContainer
        .append($("<li>")
            .append($("<span>", { text: "none", class: "btn btn-default sticker-label" })
                .click(setLabel)));

    for (var i = 0; i < data.responseJSON.AllLabels.length; i++) {
        var labelSpecificClass = data.responseJSON.AllLabels[i].Style;

        // If selected sticker has label and this label is currently processing 
        if (data.responseJSON.Label != null && data.responseJSON.Label.Id == data.responseJSON.AllLabels[i].Id) {
            labelSpecificClass += " selected-label";
        }

        // Create label and add it to list
        labelsContainer
            .append($("<li>")
                .append($("<span>",
                {
                    text: data.responseJSON.AllLabels[i].Header + "                                                 ",
                    class: "btn sticker-label " + labelSpecificClass
                })
                    .click(setLabel)));
    }

    // If selected sticker doesn't have label
    if (data.responseJSON.Label == null) {
        // Select "none" label
        labelsContainer.children().children().first().addClass("selected-label");
    }

    var labels = labelsContainer.children();
    for (var i = 0; i < labels.length; i++) {
        // Add "check" sign to label if it is marked as selected
        if ($(labels[i]).children().hasClass("selected-label")) {
            $(labels[i]).append($("<i>", { class: "fa fa-check" }));
        }
    }

    // Save all sticker info for future processing
    stickerInfo = data.responseJSON;

    //Configure dialog window
    var dialog = $("#stickerInfo").dialog({
        autoOpen: false,
        height: 400,
        width: 600,
        modal: true,
        buttons:[
            {
                text: "Close",
                class: "btn btn-default",
                click: function () {
                    dialog.dialog("close");
                }
            }]
    });
    dialog.dialog("open");
}

function setLabel(e) {
    // Remove "check" sign from labels
    $("#labels").children().children("i").remove();

    var labelListItem = $(e.currentTarget).parent();

    labelListItem.append($("<i>", { class: "fa fa-check" }));

    // Remove label from sticker in kanban
    shownSticker.children(".sticker-label").remove();

    var labelId;
    // If not "none" label was set
    if (labelListItem.index() - 1 != -1) {
        // Get setted label id
        labelId = stickerInfo.AllLabels[labelListItem.index() - 1].Id;

        // Add label to sticker in kanban
        shownSticker.prepend($("<div>", { class: e.currentTarget.className }));
    }
    else {
        labelId = null;
    }

    $.ajax(
    {
        url: SET_STICKER_LABEL_ROUTE,
        method: "POST",
        context: document.body,
        data: { stickerId: stickerInfo.Id, labelId: labelId }
    });

}

function editStickerHeader() {
    // Create sticker editor
    var editor = $("<input>",
        {
            type: "textbox",
            value: $("#stickerInfoHeader").text().trim(),
            id: "stickerInfoHeaderEditor"
        })
        .focusout(function (e) {
            var headerInput = $(e.currentTarget);

            var headerText = headerInput.val().trim();

            if (headerText.length != 0) {
                // Set info window sticker header
                headerInput
                    .replaceWith(headerText);

                // Set kanban sticker header
                shownSticker.children("h4").text(headerText);

                $.ajax({
                    url: RENAME_STICKER_ROUTE,
                    context: document.body,
                    method: "POST",
                    data: { id: stickerInfo.Id, header: headerText }
                });

            }

        })
    // Clear header
    $("#stickerInfoHeader").text("");

    $("#stickerInfoHeader").append(editor);

    editor.focus().select();
}

function editInfo(e) {
    // If now editor is not opened
    if ($("#stickerInfoEditBtn").text().trim() == "Edit") {
        // Replace description text with editor area
        $("#stickerInfoDescription").replaceWith(
            $("<textarea>",
            {
                text: (!isEmptyDescription ? $("#stickerInfoDescription").text() : ""),
                id: "stickerInfoEditor"
            }).focus());

        // Switch "Edit" button to "Save"
        $("#stickerInfoEditBtn").removeClass("btn-default").addClass("btn-primary").text("Save");
    }
    else {
        var descriptionText = $("#stickerInfoEditor").val();
        // If entered description not empty, but contains only space characters
        if (descriptionText.length != 0 && descriptionText.trim().length == 0) {
            // Clear editor text and cancel description saving
            $("#stickerInfoEditor").text(descriptionText.trim());
            return;
        }

        descriptionText = descriptionText.trim();

        $.ajax({
            url: SET_STICKER_DESCRIPTION_ROUTE,
            context: document.body,
            method: "POST",
            data: { id: $("#stickerId").val(), description: descriptionText },
            complete: function () {
                // Replace description editor with it text
                $("#stickerInfoEditor").replaceWith($("<p>", {
                    id: "stickerInfoDescription",
                    text: (text.length != 0 ? descriptionText : "There is no description.")
                }));

                // Switch "Save" button to "Edit"
                $("#stickerInfoEditBtn").removeClass("btn-primary").addClass("btn-default").text("Edit");
            }
        });
    }
}

function deleteSticker() {
    $("#stickerInfo").dialog("close");

    // Remove sticker from kanban
    shownSticker.remove();

    $.ajax({
        url: DELETE_STICKER_ROUTE,
        context: document.body,
        data: { id: stickerInfo.Id },
        method: "POST",
    });
}

function changeExecuting(e) {
    var executingButton = $(e.currentTarget);
    var isExecuting = undefined;

    // If now not executing
    if (executingButton.hasClass("btn-warning")) {
        setExecuting(executingButton);
        isExecuting = true;
    }
    // If now executing
    else if (executingButton.hasClass("btn-success")) {
        setNonExecuting(executingButton);
        isExecuting = false;
    }

    $.ajax(
        {
            url: SET_EXECUTING_STICKER_ROUTE,
            context: document.body,
            data: { id: stickerInfo.Id, isExecuting: isExecuting },
            method: "POST",
        });

}

function setExecuting(button) {
    button.addClass("btn-success").removeClass("btn-warning");
    button.children("i").removeClass("fa-remove").addClass("fa-check");
    button.children("span").text("Executing");

    $("#executors").prepend($("<div>", {
        style: "text-decoration: underline;",
        text: "You",
        id: "userExecutor"
    }));

    // Remove placeholder for empty executors list
    $("#none").remove();
}

function setNonExecuting(button) {
    button.removeClass("btn-success").addClass("btn-warning");
    button.children("i").removeClass("fa-check").addClass("fa-remove");
    button.children("span").text("Not executing");

    // Remove user executing from list
    $("#userExecutor").remove();

    // Add placeholder for empty executors list if it is empty after user removing
    if ($("#executors").children().length == 0) {
        $("#executors").append($("<div>", { text: "none", id: "none" }));
    }
}

function updateSortables() {
    $("#kanban").sortable(
        {
            connectWith: "#kanban",
            handle: ".kanban-column-header",
            update: replaceColumn,
            items: ".kanban-column",
            cancel: ".kanban-column-close"
        });

    $(".kanban-column").sortable(
        {
            connectWith: ".kanban-column",
            handle: ".subpartition-header",
            cancel: ".subpartition-remove-btn",
            receive: replaceSubcolumn,
            items: ".subpartition"
        });
    $(".subpartition").sortable(
        {
            connectWith: ".subpartition",
            items: ".sticker",
            receive: function (e, ui) {
                // If sticker was dropped after sticker adding button
                if ($(ui.item).index() == $(ui.item).parent().children().length - 1) {
                    // Move dropped sticker to the beginning of subpartition 
                    $(ui.item).insertBefore($(ui.item).parent().children(".sticker-add-btn"));
                }
            },
            out: stickerOut,
            update: replaceSticker,
            start: stickerSortStart
        })
}

$(document).ready(function () {
    $("#addColumnButton").click(addColumnButtonClick);
    $(".kanban-column-header>i").click(deleteColumn);

    $(".kanban-column-header>h3").dblclick(editColumnHeader);

    $(".subpartition-add-btn").click(addSubpartition);

    $(".subpartition-header").dblclick(editSubpartitionHeader);

    $(".subpartition-remove-btn").click(removeSubpartition);

    $(".sticker-add-btn>div").click(addSticker);

    $(".sticker").click(showInfo)

    $("#stickerInfoEditBtn").click(editInfo)

    $("#deleteBtn").click(deleteSticker);
    $("#executeBtn").click(changeExecuting);

    $("#stickerInfoHeader").dblclick(editStickerHeader);

    updateSortables();
});
